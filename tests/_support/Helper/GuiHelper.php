<?php

declare(strict_types=1);

namespace Helper;

class GuiHelper extends \Codeception\Module {

	protected $moduleBrowser;

	public function dontSeeError(): void
    {
		if (!isset($this->moduleBrowser))  {
			try {
				$this->moduleBrowser = $this->getModule('WebDriver');
			} catch (\Codeception\Exception\ModuleException $exception)  {
				$this->moduleBrowser = $this->getModule('PhpBrowser');
			}
		}

		$this->moduleBrowser->dontSee('Error:');
		$this->moduleBrowser->dontSee('Warning:');
		$this->moduleBrowser->dontSee('Notice:');
	}
}
