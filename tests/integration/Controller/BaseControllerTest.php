<?php

declare(strict_types=1);

namespace Ergo\Controller;

use Ergo\Application;
use Ergo\Config\Config;

class BaseControllerTest extends \Codeception\Test\Unit
{

    /**
     * @var \IntegrationTester
     */
    protected $tester;

    public function testHtml(): void
    {
        $application = new Application(new Config(__DIR__));
        $controller = new BaseController($application);
        $controller->html('<p>Lorem ipsum</p>');

        $this->tester->assertEquals('<p>Lorem ipsum</p>', $application->response()->getBody());
        $this->tester->assertEquals(200, $application->response()->getStatusCode());
    }

    public function testXml(): void
    {
        $application = new Application(new Config(__DIR__));
        $controller = new BaseController($application);
        $controller->xml('<output>Lorem ipsum</output>');

        $this->tester->assertEquals('<output>Lorem ipsum</output>', $application->response()->getBody());
        $this->tester->assertEquals(200, $application->response()->getStatusCode());
        $this->tester->assertEquals('application/xml', $application->response()->getHeader('Content-Type')[0]);
    }

    public function testJson(): void
    {
        $application = new Application(new Config(__DIR__));
        $controller = new BaseController($application);
        $controller->json(['output' => 'Lorem ipsum']);

        $this->tester->assertEquals(json_encode(['output' => 'Lorem ipsum']), $application->response()->getBody());
        $this->tester->assertEquals(200, $application->response()->getStatusCode());
        $this->tester->assertEquals('application/json', $application->response()->getHeader('Content-Type')[0]);
    }

    public function testFile(): void
    {
        $file = codecept_data_dir().'/dummy.txt';
        $application = new Application(new Config(__DIR__));
        $controller = new BaseController($application);
        $controller->file($file);

        $this->tester->assertEquals(file_get_contents($file), $application->response()->getBody());
        $this->tester->assertEquals(200, $application->response()->getStatusCode());
        $this->tester->assertEquals('text/plain', $application->response()->getHeader('Content-Type')[0]);
        $this->tester->assertEquals(filesize($file), $application->response()->getHeader('Content-Length')[0]);
    }

    public function testDownload(): void
    {
        $file = codecept_data_dir().'/dummy.txt';
        $application = new Application(new Config(__DIR__));
        $controller = new BaseController($application);
        $controller->download($file);

        $this->tester->assertEquals(file_get_contents($file), $application->response()->getBody());
        $this->tester->assertEquals(200, $application->response()->getStatusCode());
        $this->tester->assertEquals('text/plain', $application->response()->getHeader('Content-Type')[0]);
        $this->tester->assertEquals(filesize($file), $application->response()->getHeader('Content-Length')[0]);
        $this->tester->assertEquals('attachment; filename="dummy.txt"', $application->response()->getHeader('Content-Disposition')[0]);
    }

    public function testRedirect(): void
    {
        $application = new Application(new Config(__DIR__));
        $controller = new BaseController($application);
        $controller->redirect('http://example.org');

        $this->tester->assertEquals('', $application->response()->getBody());
        $this->tester->assertEquals(302, $application->response()->getStatusCode());
        $headers = $application->response()->getHeaders();
        $this->tester->assertEquals(['http://example.org'], $headers['Location']);
    }
}
