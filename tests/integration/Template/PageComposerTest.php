<?php

declare(strict_types=1);

namespace Ergo\Template;

use Codeception\Configuration;
use Ergo\Acl\Permissions;
use Ergo\Application;
use Ergo\Config\Config;
use Ergo\Html\Links;
use Ergo\Module\DummyModule;
use Ergo\Template\PageComposer;
use ZdenekGebauer\Router\Routes;

require_once dirname(__DIR__)  .'/Module/DummyModule.php';

class PageComposerTest extends \Codeception\Test\Unit
{

    /**
     * @var \IntegrationTester
     */
    protected $tester;

    private function getApplication(): Application
    {
        $rootDirectory = dirname(__DIR__, 3) . '/src/default_web';
        $config = new Config($rootDirectory);
        $config->addConfigFile(codecept_data_dir() . '/Config/test_config.ini');
        return new Application($config);
    }

    public function testRenderWithModuleOutput(): void
    {
        $application = $this->getApplication();
        $application->registerModule(DummyModule::class);
        $composer = new PageComposer($application);
        $composer->pageTemplate(codecept_data_dir('Template') . '/page.htm');

        $output = $composer->render('MAIN_CONTENT');
        $this->tester->assertStringContainsString('<body>MAIN_CONTENT', $output);
        $this->tester->assertStringContainsString('INLINE_A', $output);
        $this->tester->assertStringContainsString('http://ergo.local/test', $output);
    }
}
