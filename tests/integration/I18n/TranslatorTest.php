<?php

declare(strict_types=1);

namespace Ergo\I18n;

use Exception;

class TranslatorTest extends \Codeception\Test\Unit
{

    private $translations = "#comment\n\nTOKEN_1;translation1\nTOKEN_2;translation2";

    private $dir;

    protected function _before()
    {
        $this->dir = sys_get_temp_dir() . '/';
    }

    public function testTranslate(): void
    {
        $files[0] = $this->dir . 'file.txt';
        file_put_contents($files[0], $this->translations);
        $files[1] = $this->dir . 'another-file.txt';
        file_put_contents($files[1], "TOKEN_3;translation3\nINVALID");
        $translator = new Translator($files);

        $this->tester->assertEquals('translation1', $translator->translate('TOKEN_1'));
        $this->tester->assertEquals('translation2', $translator->translate('TOKEN_2'));
        $this->tester->assertEquals('translation3', $translator->translate('TOKEN_3'));

        try {
            $this->tester->assertEquals('UNKNOWN', $translator->translate('UNKNOWN'));
        } catch (Exception $exception) {
            $this->tester->assertStringContainsString('translation not found: "UNKNOWN"', $exception->getMessage());
        }
        try {
            $translator->translate('');
        } catch (Exception $exception) {
            $this->tester->assertStringContainsString('translation not found: ""', $exception->getMessage());
        }

        unlink($files[0]);
        unlink($files[1]);
    }

    public function testTranslateAll()
    {
        $files[0] = $this->dir . 'file.txt';
        file_put_contents($files[0], $this->translations);
        $translator = new Translator($files);

        $input = 'a {%TOKEN_1%} b {%TOKEN_2%} c {%TOKEN_2%}';
        $expect = 'a translation1 b translation2 c translation2';
        $this->tester->assertEquals($expect, $translator->translateAll($input));

        unlink($files[0]);
    }

}
