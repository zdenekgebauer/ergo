<?php

declare(strict_types=1);

namespace Ergo\Module;

use DateTimeInterface;
use Ergo\Acl\Permissions;
use Ergo\Application;
use Ergo\Config\Config;
use Ergo\Html\Links;
use Ergo\Search\SearchIndexInterface;
use Ergo\Sitemap\SitemapInterface;
use ZdenekGebauer\Router\Routes;

class ModuleTest extends \Codeception\Test\Unit
{

    /**
     * @var \IntegrationTester
     */
    protected $tester;

    public function testDefaultParameters(): void
    {
        $application = new Application(new Config(__DIR__));
        $module = new class($application) extends Module {
            public function name(): string
            {
                return 'dummy';
            }

            public function internalName(): string
            {
                return 'dummy';
            }
        };
        $this->tester->assertEquals(new Routes(), $module->routes());
        $this->tester->assertEquals('', $module->inlineContent('undefined'));
        $this->tester->assertEquals(new Permissions(), $module->backendPermissions());
        $this->tester->assertEquals(new Links(), $module->backendMenu());
        $this->tester->assertNull($module->cron());

        $sitemap = new class implements SitemapInterface {

            public function addItems(string $moduleClassName, array $items): void
            {
            }

            public function removeOldItems(string $moduleClassName, DateTimeInterface $limit): void
            {
            }
        };
        $this->tester->assertNull($module->updateSitemap($sitemap));

        $search = new class implements SearchIndexInterface {
            public function addItems(string $moduleClassName, array $items): void
            {
            }
        };
        $this->tester->assertNull($module->updateSearchIndex($search));
    }
}
