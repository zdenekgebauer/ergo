<?php

declare(strict_types=1);

namespace Ergo\Module;

use ZdenekGebauer\Router\Route;
use ZdenekGebauer\Router\Routes;

class DummyModule extends Module implements ModuleInterface
{

    public function routes(): Routes
    {
        return new Routes(
            new Route('/', [], function()  {
                $this->application->setResponse($this->application->response()->html('output of module', 200));
            }, 'home'),
            new Route('/test', [], function()  {
                $this->application->setResponse($this->application->response()->html('test output', 200));
            }, 'test')
        );
    }

    public function inlineContent(string $parameter): string
    {
        if ($parameter === '_a') {
            return 'INLINE_A';
        }
        return parent::inlineContent($parameter);
    }

    public function name(): string
    {
        return 'dummy';
    }

    public function internalName(): string
    {
        return 'dummy';
    }
}
