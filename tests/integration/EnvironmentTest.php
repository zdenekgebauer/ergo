<?php

declare(strict_types=1);

namespace Ergo;

use Ergo\Config\Config;
use Tracy\Debugger;

use function dirname;

class EnvironmentTest extends \Codeception\Test\Unit
{

    /**
     * @var \IntegrationTester
     */
    protected $tester;

    public function testSetEnvironment(): void
    {
        $config = new Config(__DIR__);
        $config->addParameter('timezone', 'Europe/London');
        Environment::setEnvironment($config);

        $this->tester->assertEquals($config->defaultTimezone(), date_default_timezone_get());
        $this->tester->assertEquals('0', ini_get('session.cookie_secure'));

        $_SERVER['SERVER_PORT'] = 443;
        Environment::setEnvironment($config);
        $this->tester->assertEquals('1', ini_get('session.cookie_secure'));
        unset($_SERVER['SERVER_PORT']);
    }

    public function testSetErrorHandlers(): void
    {
        $oldErrorHandler = set_error_handler(static function () {
        });
        restore_error_handler();

        $rootDirectory = dirname(__DIR__, 2) . '/src/default_web';
        $config = new Config($rootDirectory);
        // TODO ZG doplnit konstanty z config.ini
        $config->addParameter('developer_ip', ['secret@127.0.0.1', 'other@192.168.0.1']);
        $config->addParameter('error_email', ['test@example.org', 'test2@example.org']);

        $_SERVER['HTTP_X_REQUESTED_WITH'] = 'xmlhttprequest';
        Environment::setErrorHandlers($config);
        $this->tester->assertEquals(E_ALL, Debugger::$logSeverity);
        $this->tester->assertEquals($rootDirectory . '/log/err/', Debugger::$logDirectory);
        $this->tester->assertNotEmpty($_SERVER['HTTP_X_TRACY_AJAX']);
        set_error_handler($oldErrorHandler);
        unset($_SERVER['HTTP_X_REQUESTED_WITH'], $_SERVER['HTTP_X_TRACY_AJAX']);
    }
}
