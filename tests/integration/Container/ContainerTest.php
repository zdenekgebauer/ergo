<?php

declare(strict_types=1);

namespace Ergo\Container;

class ContainerTest extends \Codeception\Test\Unit
{

    /**
     * @var \IntegrationTester
     */
    protected $tester;

    public function testGetWithInvalidClass(): void
    {
        $container = new Container();
        $this->expectException(NotFoundException::class);
        $this->expectExceptionMessage('Class "NotExists" does not exist');
        $container->get('NotExists');
    }

    public function testHas(): void
    {
        $container = new Container();
        $this->tester->assertTrue($container->has(DummyClass::class));
        $this->tester->assertTrue($container->has(DummyInterface::class));
        $this->tester->assertFalse($container->has('NotExists'));
    }

    public function testGetWithInterface(): void
    {
        $container = new Container();
        $this->expectException(ContainerException::class);
        $this->expectExceptionMessage('Cannot instantiate interface');
        $container->get(DummyInterface::class);
    }

    public function testAddSubstitute(): void
    {
        $container = new Container();
        $dummyClass = new DummyClass();

        $container->addSubstitute(DummyInterface::class, $dummyClass);
        $dummyService = $container->get(DummyService::class);
        $this->tester->assertInstanceOf(DummyService::class, $dummyService);
        $this->tester->assertSame($dummyClass, $dummyService->dummy);
        $this->tester->assertTrue($container->has(DummyService::class));
        $this->tester->assertTrue($container->has(DummyClass::class));
        $this->tester->assertTrue($container->has(DummyInterface::class));
    }

    public function testSet(): void
    {
        $container = new Container();
        $dummyClass = new DummyClass();

        $container->set('dummy', $dummyClass);
        $this->tester->assertSame($dummyClass, $container->get('dummy'));

        $container->set('dummy2');
        $this->tester->assertSame('dummy2', $container->get('dummy2'));
    }

    public function testHasInstance(): void
    {
        $container = new Container();
        $this->tester->assertFalse($container->hasInstance(DummyClass::class));
        $container->set(DummyClass::class, new DummyClass());
        $this->tester->assertTrue($container->hasInstance(DummyClass::class));

        $this->tester->assertFalse($container->has('NotExists'));
    }
}

interface DummyInterface
{

}

class DummyClass implements DummyInterface
{

}

class DummyService
{
    public $dummy;

    public function __construct(DummyInterface $dummy)
    {
        $this->dummy = $dummy;
    }
}
