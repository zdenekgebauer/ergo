<?php

declare(strict_types=1);

namespace Ergo;

require_once __DIR__ . '/Module/DummyModule.php';

use Codeception\Configuration;
use Ergo\Config\Config;
use Ergo\Http\Request;
use Ergo\Http\Response;
use Ergo\Module\DummyModule;
use InvalidArgumentException;
use function dirname;

class ApplicationTest extends \Codeception\Test\Unit
{

    /**
     * @var \IntegrationTester
     */
    protected $tester;

    private function getApplication(): Application
    {
        $rootDirectory = dirname(__DIR__, 2) . '/src/default_web';
        $config = new Config($rootDirectory);
        $config->addConfigFile(Configuration::dataDir() . '/Config/test_config.ini');
        return new Application($config);
    }

    public function testRun(): void
    {
        $response = $this->getApplication()->run(new Request('GET', '/'));
        $this->tester->assertInstanceOf(Response::class, $response);
        $this->tester->assertStringContainsString('<h1>default page</h1>', $response->getBody());
        $this->tester->assertEquals(200, $response->getStatusCode());
    }

    public function testRender404Page(): void
    {
        $response = $this->getApplication()->run(new Request('GET', '/not-found'));
        $this->tester->assertInstanceOf(Response::class, $response);
        $this->tester->assertStringContainsString('<h1>page not found</h1>', $response->getBody());
        $this->tester->assertEquals(404, $response->getStatusCode());
    }

    public function testRegisterModule(): void
    {
        $application = $this->getApplication();
        $application->registerModule(DummyModule::class);
        $this->tester->assertCount(1,   $application->getModules());
        $this->tester->assertInstanceOf(DummyModule::class, $application->module('dummy'));
        $response = $application->run(new Request('GET', '/'));

        $this->tester->assertInstanceOf(Response::class, $response);
        $this->tester->assertInstanceOf(Request::class, $application->request());
        $this->tester->assertEquals('output of module', (string)$response->getBody());
        $this->tester->assertEquals(200, $response->getStatusCode());

        $this->tester->expectThrowable(
            new InvalidArgumentException('unknown module "undefined"'),
            static function () use ($application) {
                $application->module('undefined');
            }
        );
    }

    public function testUrlAbsolute(): void
    {
        $application = $this->getApplication();
        $application->registerModule(DummyModule::class);

        $this->tester->assertEquals('http://ergo.local/', $application->urlAbsolute('home'));
        $this->tester->assertEquals('http://ergo.local/test', $application->urlAbsolute('test'));
    }
}
