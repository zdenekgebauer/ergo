<?php

declare(strict_types=1);

namespace Ergo\Http;

class RequestTest extends \Codeception\Test\Unit
{

    /**
     * @var \IntegrationTester
     */
    protected $tester;

    public function testDefaultValues(): void
    {
        $request = Request::fromGlobals();
        $this->tester->assertInstanceOf(Request::class, $request);

        $this->tester->assertEquals('http://localhost', $request->currentUrl());

        // string
        $this->tester->assertEquals('', $request->getQuery('variable'));
        $this->tester->assertEquals('', $request->getPost('variable'));
        $this->tester->assertEquals('', $request->getCookie('variable'));
        $this->tester->assertEquals('', $request->getPostHtml('variable'));

        // int
        $this->tester->assertIsInt($request->getQueryInt('variable'));
        $this->tester->assertEquals(0, $request->getQueryInt('variable'));
        $this->tester->assertIsInt($request->getPostInt('variable'));
        $this->tester->assertEquals(0, $request->getPostInt('variable'));

        // float
        $this->tester->assertIsFloat($request->getQueryFloat('variable'));
        $this->tester->assertEquals(0, $request->getQueryFloat('variable'));
        $this->tester->assertIsFloat($request->getPostFloat('variable'));
        $this->tester->assertEquals(0, $request->getPostFloat('variable'));

        // bool
        $this->tester->assertIsBool($request->getQueryBool('variable'));
        $this->tester->assertFalse($request->getQueryBool('variable'));
        $this->tester->assertIsBool($request->getPostBool('variable'));
        $this->tester->assertFalse( $request->getPostBool('variable'));

        // date
        $this->tester->assertNull($request->getQueryDate('variable'));
        $this->tester->assertNull($request->getPostDate('variable'));

        // array
        $this->tester->assertEquals([], $request->getPostArray('variable'));
        $this->tester->assertEquals([], $request->getPostArrayInt('variable'));

        $this->tester->assertFalse($request->hasPost());
        $this->tester->assertFalse($request->isAjax());
    }

    public function testValuesFromGlobals(): void
    {
        $_GET['variable'] = '<p> Lorem ipsum </p>';
        $_POST['variable'] = ' <p> dolor sit amet </p> ';
        $_COOKIE['variable'] = '<p> dolor sit amet </p>';

        $_GET['count'] = '1 234,45';
        $_POST['count'] = '2 234,45';

        $_GET['bool_true1'] = '1';
        $_GET['bool_true2'] = 'true';
        $_GET['bool_true3'] = 'yes';
        $_GET['bool_true4'] = 'on';
        $_GET['bool_false1'] = '0';
        $_GET['bool_false2'] = 'false';
        $_GET['bool_false3'] = '';
        $_GET['bool_false4'] = 'no';

        $_POST['bool_true1'] = '1';
        $_POST['bool_true2'] = 'true';
        $_POST['bool_true3'] = 'yes';
        $_POST['bool_true4'] = 'on';
        $_POST['bool_false1'] = '0';
        $_POST['bool_false2'] = 'false';
        $_POST['bool_false3'] = '';
        $_POST['bool_false4'] = 'no';

        $_POST['date_invalid'] = 'invalid_date';
        $_POST['date_us'] = '3/30/2019 11:12:13';
        $_POST['date_iso'] = '20191001T102030';
        $_POST['date_cs'] = '4. 12. 2019 09:19:29';

        $_POST['string_array'] = ['', ' lorem ', 123, 'true'];
        $_POST['int_array'] = ['', ' 2 345,6 ', 10, 0, -10, 'true'];

        $_SERVER['HTTP_X_REQUESTED_WITH'] = 'XmlHttpRequest';
        $_SERVER['HTTP_X_PURPOSE'] = 'Preview';

        $request = Request::fromGlobals();

        // string
        $this->tester->assertEquals('Lorem ipsum', $request->getQuery('variable'));
        $this->tester->assertEquals('dolor sit amet', $request->getPost('variable'));
        $this->tester->assertEquals('dolor sit amet', $request->getCookie('variable'));
        $this->tester->assertEquals('<p> dolor sit amet </p>', $request->getPostHtml('variable'));

        // int
        $this->tester->assertEquals(1234, $request->getQueryInt('count'));
        $this->tester->assertEquals(2234, $request->getPostInt('count'));

        // float
        $this->tester->assertEquals(1234.45, $request->getQueryFloat('count'));
        $this->tester->assertEquals(2234.45, $request->getPostFloat('count'));

        // bool
        $this->tester->assertTrue( $request->getQueryBool('bool_true1'));
        $this->tester->assertTrue( $request->getQueryBool('bool_true2'));
        $this->tester->assertTrue( $request->getQueryBool('bool_true3'));
        $this->tester->assertTrue( $request->getQueryBool('bool_true4'));
        $this->tester->assertFalse( $request->getQueryBool('bool_false1'));
        $this->tester->assertFalse( $request->getQueryBool('bool_false2'));
        $this->tester->assertFalse( $request->getQueryBool('bool_false3'));
        $this->tester->assertFalse( $request->getQueryBool('bool_false4'));

        $this->tester->assertTrue( $request->getPostBool('bool_true1'));
        $this->tester->assertTrue( $request->getPostBool('bool_true2'));
        $this->tester->assertTrue( $request->getPostBool('bool_true3'));
        $this->tester->assertTrue( $request->getPostBool('bool_true4'));
        $this->tester->assertFalse( $request->getPostBool('bool_false1'));
        $this->tester->assertFalse( $request->getPostBool('bool_false2'));
        $this->tester->assertFalse( $request->getPostBool('bool_false3'));
        $this->tester->assertFalse( $request->getPostBool('bool_false4'));

        // date
        $this->tester->assertNull( $request->getPostDate('date_invalid'));
        $usDate = $request->getPostDate('date_us');
        $this->tester->assertInstanceOf(\DateTimeImmutable::class, $usDate);
        $this->tester->assertEquals(2019 , $usDate->format('Y'));
        $this->tester->assertEquals('03' , $usDate->format('m'));
        $this->tester->assertEquals('30' , $usDate->format('d'));
        $this->tester->assertEquals('11' , $usDate->format('H'));
        $this->tester->assertEquals('12' , $usDate->format('i'));
        $this->tester->assertEquals('13' , $usDate->format('s'));

        $isoDate = $request->getPostDate('date_iso');
        $this->tester->assertInstanceOf(\DateTimeImmutable::class, $isoDate);
        $this->tester->assertEquals(2019 , $isoDate->format('Y'));
        $this->tester->assertEquals('10' , $isoDate->format('m'));
        $this->tester->assertEquals('01' , $isoDate->format('d'));
        $this->tester->assertEquals('10' , $isoDate->format('H'));
        $this->tester->assertEquals('20' , $isoDate->format('i'));
        $this->tester->assertEquals('30' , $isoDate->format('s'));

        $csDate = $request->getPostDate('date_cs');
        $this->tester->assertInstanceOf(\DateTimeImmutable::class, $csDate);
        $this->tester->assertEquals(2019 , $csDate->format('Y'));
        $this->tester->assertEquals('12' , $csDate->format('m'));
        $this->tester->assertEquals('04' , $csDate->format('d'));
        $this->tester->assertEquals('09' , $csDate->format('H'));
        $this->tester->assertEquals('19' , $csDate->format('i'));
        $this->tester->assertEquals('29' , $csDate->format('s'));

        $this->tester->assertEquals(['', 'lorem', '123', 'true'] , $request->getPostArray('string_array'));
        $this->tester->assertEquals([0, 2345, 10, 0, -10, 0] , $request->getPostArrayInt('int_array'));

        $this->tester->assertTrue($request->isAjax());
    }
}
