<?php

declare(strict_types=1);

namespace Ergo\Http;

class ResponseTest extends \Codeception\Test\Unit
{

    /**
     * @var \IntegrationTester
     */
    protected $tester;

    public function testHtml(): void
    {
        $response = new Response();
        $output = $response->html('<p>HTML output</p>');
        $this->tester->assertEquals(200, $output->getStatusCode());
        $this->tester->assertEquals('<p>HTML output</p>', $output->getBody());
        $this->tester->assertEquals('text/html; charset=utf-8', $output->getHeader('Content-Type')[0]);
    }

    public function testXml(): void
    {
        $response = new Response();
        $output = $response->xml('<output>XML output</output>');
        $this->tester->assertEquals(200, $output->getStatusCode());
        $this->tester->assertEquals('<output>XML output</output>', $output->getBody());
        $this->tester->assertEquals('application/xml', $output->getHeader('Content-Type')[0]);
    }

    public function testJson(): void
    {
        $response = new Response();
        $output = $response->json(['output' => 'json']);
        $this->tester->assertEquals(200, $output->getStatusCode());
        $this->tester->assertEquals(json_encode(['output' => 'json']), $output->getBody());
        $this->tester->assertEquals('application/json', $output->getHeader('Content-Type')[0]);
    }

    public function testRedirect(): void
    {
        $response = new Response();
        $output = $response->redirect('http://example.org');
        $this->tester->assertEquals(302, $output->getStatusCode());
        $this->tester->assertEquals('', $output->getBody());
        $this->tester->assertEquals(['http://example.org'], $output->getHeaders()['Location']);
    }

    public function testFile(): void
    {
        $file = codecept_data_dir().'/dummy.txt';
        $response = new Response();
        $output = $response->file($file);
        $this->tester->assertEquals(200, $output->getStatusCode());
        $this->tester->assertEquals(file_get_contents($file), $output->getBody());
        $this->tester->assertEquals(filesize($file), $output->getHeader('Content-Length')[0]);
        $this->tester->assertEquals('text/plain', $output->getHeader('Content-Type')[0]);
    }

    public function testDownload(): void
    {
        $file = codecept_data_dir().'/dummy.txt';
        $response = new Response();
        $output = $response->download($file);
        $this->tester->assertEquals(200, $output->getStatusCode());
        $this->tester->assertEquals(file_get_contents($file), $output->getBody());
        $this->tester->assertEquals(filesize($file), $output->getHeader('Content-Length')[0]);
        $this->tester->assertEquals('text/plain', $output->getHeader('Content-Type')[0]);
        $this->tester->assertEquals('attachment; filename="dummy.txt"', $output->getHeader('Content-Disposition')[0]);
    }

    public function testDownloadData(): void
    {
        $response = new Response();
        $data = '1;"Lorem ipsum"';
        $output = $response->downloadData($data, 'file.csv');
        $this->tester->assertEquals(200, $output->getStatusCode());
        $this->tester->assertEquals($data, $output->getBody());
        $this->tester->assertEquals(strlen($data), $output->getHeader('Content-Length')[0]);
        $this->tester->assertEquals('text/csv', $output->getHeader('Content-Type')[0]);
        $this->tester->assertEquals('attachment; filename="file.csv"', $output->getHeader('Content-Disposition')[0]);
    }
}
