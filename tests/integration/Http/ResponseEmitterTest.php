<?php

declare(strict_types=1);

namespace Ergo\Http;

use Codeception\Test\Unit;
use GuzzleHttp\Psr7\Response;

class ResponseEmitterTest extends Unit
{

    /**
     * @var \IntegrationTester
     */
    protected $tester;

    /**
     * @var ResponseEmitter
     */
    private $emitter;

    protected function _before()
    {
        $this->emitter = new class extends ResponseEmitter
        {

            public function assertNoPreviousOutput(): void
            {
            }

            public function emitHeader(string $header, bool $replace, ?int $statusCode = null): void
            {
            }
        };
    }

    public function testEmit(): void
    {
        $response = new Response(200, ['Content-Type' => 'text/html'], 'Lorem ipsum');
        $this->emitter->emit($response);
        $output = $this->getActualOutput();
        $this->tester->assertStringContainsString('Lorem ipsum', $output);
    }

    public function testEmitWithoutBuffer(): void
    {
        $response = new Response(200, [], 'Lorem ipsum');
        $this->emitter->emit($response, 0);
        $output = $this->getActualOutput();
        $this->tester->assertStringContainsString('Lorem ipsum', $output);
    }
}
