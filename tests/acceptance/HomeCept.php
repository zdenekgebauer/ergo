<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('test homepage');

$I->amOnPage('/');
$I->seeResponseCodeIs(200);
$I->dontSeeError();
$I->see('Ergo', 'header');
$I->see('default page', 'h1');
$I->seeLink('home');
