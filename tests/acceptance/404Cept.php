<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('test page 404');

$I->amOnPage('/not-found');
$I->seeResponseCodeIs(404);
$I->dontSeeError();
$I->see('Ergo', 'header');
$I->see('page not found', 'h1');
$I->seeLink('home');
