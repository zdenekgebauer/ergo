<?php

declare(strict_types=1);

namespace Ergo;

use Ergo\Config\Config;
use InvalidArgumentException;
use stdClass;

class ApplicationUnitTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testRegisterModuleWithInvalidClass(): void
    {
        $application = new Application(new Config(__DIR__));

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('class stdClass must implement Ergo\Module\ModuleInterface');
        $application->registerModule(stdClass::class);
    }
}
