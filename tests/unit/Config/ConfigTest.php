<?php

declare(strict_types=1);

namespace Ergo\Config;

use InvalidArgumentException;
use RuntimeException;

class ConfigTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testDefaultParameters(): void
    {
        $config = new Config(__DIR__);
        $this->tester->assertEquals(__DIR__ . '/', $config->baseDir());

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('configuration "root_url" not found');
        $this->tester->assertEquals('', $config->baseUrl());
        $this->tester->assertTrue($config->isDevelopment());
        $this->tester->assertFalse($config->isProduction());
        $this->tester->assertEquals(date_default_timezone_get(), $config->defaultTimezone());
        $this->tester->assertEquals(__DIR__ . 'log/err/', $config->errorLogDirectory());
        $this->tester->assertEquals(__DIR__ . 'log/', $config->logsDir());
        $this->tester->assertEquals([], $config->errorEmails());
        $this->tester->assertEquals([], $config->developerCookies());
    }

    public function testAddConfigFileWithMissingFile(): void
    {
        $config = new Config(__DIR__);

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('not found file nofile.ini');
        $config->addConfigFile('nofile.ini');
    }

    public function testAddConfigFileWithEmptyFile(): void
    {
        $config = new Config(__DIR__);

        $config->addConfigFile(codecept_data_dir() . 'Config/empty_config.ini');
        $this->tester->assertTrue($config->isDevelopment());
    }

    public function testAddConfigFile(): void
    {
        $config = new Config(__DIR__);

        $config->addConfigFile(codecept_data_dir() . 'Config/test_config.ini');
        $this->tester->assertEquals(__DIR__ . '/', $config->baseDir());
        $this->tester->assertEquals('http://ergo.local/', $config->baseUrl());
        $this->tester->assertTrue($config->isProduction());
        $this->tester->assertEquals('Europe/London', $config->defaultTimezone());
        $this->tester->assertEquals(__DIR__ . '/log/', $config->getValue(ConfigInterface::PARAMETER_LOG_DIRECTORY));
        $this->tester->assertEquals(['ergo@example.org', 'ergo2@example.org'], $config->getValueAsArray('error_email'));
        $this->tester->assertEquals(['cookievalue@192.168.0.1'], $config->getValueAsArray('developer_ip'));
    }

    public function testSetRunModeInvalid(): void
    {
        $config = new Config(__DIR__);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('invalid value "invalid" for "mode"');
        $config->addParameter(ConfigInterface::PARAMETER_MODE, 'invalid');
    }

    public function testGetValueAsArrayInvalid(): void
    {
        $config = new Config(__DIR__);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('configuration "notexists" not found');
        $config->getValueAsArray('notexists');
    }

    public function testGetValueWithArray(): void
    {
        $config = new Config(__DIR__);
        $config->addParameter('arrayParameter', []);

        $this->expectException(\BadMethodCallException::class);
        $this->expectExceptionMessage('configuration "arrayParameter" is array, use method getValueAsArray');
        $config->getValue('arrayParameter');
    }
}
