<?php

declare(strict_types=1);

namespace Ergo\Acl;

use DateTime;
use Ergo\Traits\PropertyCreatedAt;
use Ergo\Traits\PropertyDate;
use Ergo\Traits\PropertyMetaDescription;
use Ergo\Traits\PropertyName;
use Ergo\Traits\PropertyPerex;
use Ergo\Traits\PropertyPublished;
use Ergo\Traits\PropertySlug;
use Ergo\Traits\PropertyText;
use Ergo\Traits\PropertyTitle;
use Ergo\Traits\PropertyUpdatedAt;

class PropertyTraitsTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testPropertyCreatedAt(): void
    {
        $obj = new class {
            use PropertyCreatedAt;
        };

        $this->tester->assertNull($obj->getCreatedAt());
        $now = new DateTime();
        $obj->setCreatedAt($now);
        $this->tester->assertEquals($now, $obj->getCreatedAt());

        $obj->setCreatedAt(null);
        $this->tester->assertNull($obj->getCreatedAt());
    }

    public function testPropertyUpdatedAt(): void
    {
        $obj = new class {
            use PropertyUpdatedAt;
        };

        $this->tester->assertNull($obj->getUpdatedAt());
        $now = new DateTime();
        $obj->setUpdatedAt($now);
        $this->tester->assertEquals($now, $obj->getUpdatedAt());

        $obj->setUpdatedAt(null);
        $this->tester->assertNull($obj->getUpdatedAt());
    }

    public function testPropertyDate(): void
    {
        $obj = new class {
            use PropertyDate;
        };

        $this->tester->assertNull($obj->getDate());
        $this->tester->assertEquals('', $obj->getDateFormatted('c'));

        $now = new DateTime();
        $obj->setDate($now);
        $this->tester->assertEquals($now, $obj->getDate());
        $this->tester->assertEquals(date('Y-m-d'), $obj->getDateFormatted('Y-m-d'));

        $obj->setDate(null);
        $this->tester->assertNull($obj->getDate());
    }

    public function testPropertyMetaDescription(): void
    {
        $obj = new class {
            use PropertyMetaDescription;
        };

        $this->tester->assertEquals('', $obj->getMetaDescription());
        $this->tester->assertEquals('', $obj->getMetaKeywords());

        $obj->setMetaDescription(str_repeat('description', 30));
        $obj->setMetaKeywords(str_repeat('keywords', 30));
        $this->tester->assertEquals(185, strlen($obj->getMetaDescription()));
        $this->tester->assertEquals(185, strlen($obj->getMetaKeywords()));
    }

    public function testPropertyTitle(): void
    {
        $obj = new class {
            use PropertyTitle;
        };

        $this->tester->assertEquals('', $obj->getTitle());

        $obj->setTitle(str_repeat('title', 30));
        $this->tester->assertEquals(100, strlen($obj->getTitle()));
    }

    public function testPropertyName(): void
    {
        $obj = new class {
            use PropertyName;
        };

        $this->tester->assertEquals('', $obj->getName());

        $obj->setName(str_repeat('name', 30));
        $this->tester->assertEquals(100, strlen($obj->getName()));
    }

    public function testPropertySlog(): void
    {
        $obj = new class {
            use PropertySlug;
        };

        $this->tester->assertEquals('', $obj->getSlug());

        $obj->setSlug(str_repeat('slug-', 30));
        $this->tester->assertEquals(102, strlen($obj->getSlug()));
    }

    public function testPropertyPerex(): void
    {
        $obj = new class {
            use PropertyPerex;
        };

        $this->tester->assertEquals('', $obj->getPerex());

        $obj->setPerex('perex');
        $this->tester->assertEquals('perex', $obj->getPerex());
    }

    public function testPropertyText(): void
    {
        $obj = new class {
            use PropertyText;
        };

        $this->tester->assertEquals('', $obj->getText());

        $obj->setText('text');
        $this->tester->assertEquals('text', $obj->getText());
    }

    public function testPropertyPublished(): void
    {
        $obj = new class {
            use PropertyPublished;
        };

        $this->tester->assertFalse($obj->isPublished());

        $obj->setPublished(true);
        $this->tester->assertTrue($obj->isPublished());
    }
}

