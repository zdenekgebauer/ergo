<?php

declare(strict_types=1);

namespace Ergo\Search;

class IndexItemTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testConstructor(): void
    {
        $url = 'http://example.org';
        $now = new \DateTime();

        $item = new IndexItem($url, $now);
        $this->tester->assertEquals($url, $item->getUrl());
        $this->tester->assertEquals($now, $item->getUpdatedAt());

        $item = new IndexItem($url);
        $this->tester->assertEquals($url, $item->getUrl());
        $this->tester->assertNull($item->getUpdatedAt());
    }
}
