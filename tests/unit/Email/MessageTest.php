<?php

declare(strict_types=1);

namespace Ergo\Email;

class MessageTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testConstructor(): void
    {
        $sender = new Address('sender@example.org', 'Sender');
        $recipient1 = new Address('recipient1@example.org', 'Recipient 1');
        $recipient2 = new Address('recipient2@example.org', 'Recipient 2');
        $subject = 'Message body';
        $plaintext = 'Message body - plaintext';
        $html = 'Message body - <strong>HTML</strong>';

        $message = new Message($sender, $recipient1, $subject, $plaintext);
        $this->tester->assertEquals($sender, $message->getSender());
        $this->tester->assertEquals([$recipient1], $message->getRecipients());
        $this->tester->assertEquals($subject, $message->getSubject());
        $this->tester->assertEquals($plaintext, $message->getBodyPlaintext());
        $this->tester->assertEquals('', $message->getBodyHtml());

        $message = new Message($sender, [$recipient1, $recipient2], $subject, $plaintext, $html);
        $this->tester->assertEquals($sender, $message->getSender());
        $this->tester->assertEquals([$recipient1, $recipient2], $message->getRecipients());
        $this->tester->assertEquals($subject, $message->getSubject());
        $this->tester->assertEquals($plaintext, $message->getBodyPlaintext());
        $this->tester->assertEquals($html, $message->getBodyHtml());

        $this->expectErrorMessage('must be of type Ergo\Email\Address');
        new Message($sender, [$recipient1, 'invalid'], $subject, $plaintext, $html);
    }
}
