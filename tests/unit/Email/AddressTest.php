<?php

declare(strict_types=1);

namespace Ergo\Email;

class AddressTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testConstructor(): void
    {
        $email = 'test@example.org';

        $address = new Address($email, 'John Doe');
        $this->tester->assertEquals($email, $address->getEmail());
        $this->tester->assertEquals('John Doe', $address->getName());

        $address = new Address($email);
        $this->tester->assertEquals($email, $address->getEmail());
        $this->tester->assertEquals('', $address->getName());
    }
}
