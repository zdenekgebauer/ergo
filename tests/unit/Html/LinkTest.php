<?php

declare(strict_types=1);

namespace Ergo\Html;

class LinkTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testConstructor(): void
    {
        $name = 'link name';
        $url = 'http://example.org/';
        $title = 'link title';

        $link = new Link($name, $url, $title);
        $this->tester->assertEquals($name, $link->getName());
        $this->tester->assertEquals($url, $link->getUrl());
        $this->tester->assertEquals($title, $link->getTitle());

        $link = new Link($name, $url);
        $this->tester->assertEquals($name, $link->getName());
        $this->tester->assertEquals($url, $link->getUrl());
        $this->tester->assertEquals('', $link->getTitle());
    }
}
