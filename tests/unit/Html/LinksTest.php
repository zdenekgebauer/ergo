<?php

declare(strict_types=1);

namespace Ergo\Html;

class LinksTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testConstructor(): void
    {
        $links = new Links(
            new Link('Link1', 'http://example.org/'),
            new Link('Link2', 'http://example.org/'),
            new Link('Link2', 'http://example.org/')
        );
        $this->tester->assertCount(3, $links);
    }
}
