<?php

declare(strict_types=1);

namespace Ergo\Acl;

class PermissionsTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testConstructor(): void
    {
        $permissions = new Permissions(
            new Permission('id1', 'permission1'),
            new Permission('id2', 'permission2')
        );
        $this->tester->assertCount(2, $permissions);
    }
}
