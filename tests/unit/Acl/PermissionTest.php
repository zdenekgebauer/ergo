<?php

declare(strict_types=1);

namespace Ergo\Acl;

class PermissionTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testConstructor(): void
    {
        $permission = new Permission('id1', 'permission1');
        $this->tester->assertEquals('id1', $permission->getId());
        $this->tester->assertEquals('permission1', $permission->getDescription());
    }
}
