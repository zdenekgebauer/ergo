<?php

declare(strict_types=1);

namespace Ergo\Acl;

use LogicException;

class AclTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testRegisterPermissionsTwice(): void
    {
        $acl = new Acl();
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Permission "id1" is already registered');
        $acl->registerPermissions(
            new Permissions(
                new Permission('id1', 'permission1'),
                new Permission('id1', 'permission2')
            )
        );
    }

    public function testHasPermissions(): void
    {
        $acl = new Acl();
        $acl->registerPermissions(
            new Permissions(
                new Permission('id1', 'permission1'),
                new Permission('id2', 'permission2')
            )
        );
        $this->tester->assertCount(2, $acl->allPermissions());

        $acl->grantPermission(1, ['id1']);
        $acl->grantPermission(2, ['id1', 'id2', 'id3']);
        $acl->grantPermission(3, []);

        $this->tester->assertTrue($acl->hasPermission(1, 'id1'));
        $this->tester->assertFalse($acl->hasPermission(1, 'id2'));
        $this->tester->assertTrue($acl->hasPermission(2, 'id1'));
        $this->tester->assertTrue($acl->hasPermission(2, 'id2'));
        $this->tester->assertFalse($acl->hasPermission(2, 'id3'));
        $this->tester->assertFalse($acl->hasPermission(3, 'id1'));
    }
}

