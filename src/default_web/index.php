<?php

declare(strict_types=1);

namespace Ergo;

use Ergo\Config\Config;
use Ergo\Http\Request;
use Ergo\Http\Response;
use Ergo\Http\ResponseEmitter;
use Throwable;
use Tracy\Debugger;

require_once dirname(__DIR__, 2) . '/vendor/autoload.php';

$responseEmitter = new ResponseEmitter();

try {
    $config = new Config(__DIR__);
    $config->addConfigFile(__DIR__ . '/app/config/config.ini');
    Environment::setEnvironment($config);
    // uncomment if session is required
//    if (session_status() === PHP_SESSION_NONE) {
//        session_start();
//    }
    Environment::setErrorHandlers($config);

    $application = new Application($config);
    $responseEmitter->emit($application->run(Request::fromGlobals()));
} catch (Throwable $exception) {
    Debugger::$logDirectory = __DIR__ . '/log/err';
    Debugger::log($exception);
    if (isset($config) && $config->isDevelopment()) {
        throw $exception;
    }
    $output = (string)file_get_contents(__DIR__ . '/app/templates/default/500.htm');
    if (
        isset($_SERVER['HTTP_X_REQUESTED_WITH'])
        && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest'
    ) {
        $output = 'Internal Server Error';
    }
    $responseEmitter->emit(new Response(Response::HTTP_INTERNAL_SERVER_ERROR, [], $output));
}
