<?php

declare(strict_types=1);

namespace Ergo\Search;

use DateTimeInterface;

class IndexItem
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var DateTimeInterface
     */
    private $updatedAt;

    /**
     * @param string $url absolute url
     * @param DateTimeInterface|null $updatedAt
     */
    public function __construct(string $url, ?DateTimeInterface $updatedAt = null)
    {
        $this->url = $url;
        $this->updatedAt = $updatedAt;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }
}
