<?php

declare(strict_types=1);

namespace Ergo\Search;

interface SearchIndexInterface
{
    /**
     * @param string $moduleClassName
     * @param array<IndexItem> $items
     */
    public function addItems(string $moduleClassName, array $items): void;
}
