<?php

declare(strict_types=1);

namespace Ergo\Module;

use Ergo\Acl\Permissions;
use Ergo\Acl\Resource;
use Ergo\Html\Links;
use Ergo\Search\SearchIndexInterface;
use Ergo\Sitemap\SitemapInterface;
use ZdenekGebauer\Router\Routes;

interface ModuleInterface
{
    public function name(): string;

    /**
     * internal name of module
     * max 30 chars, only ascii
     */
    public static function internalName(): string;

    public function routes(): Routes;

    public function inlineContent(string $parameter): string;

    public function cron(): void;

    public function backendPermissions(): ?Permissions;

    public function frontendMenu(): ModuleRoutes;

    public function updateSitemap(SitemapInterface $sitemap): void;

    public function updateSearchIndex(SearchIndexInterface $searchIndex): void;
}
