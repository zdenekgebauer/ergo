<?php

declare(strict_types=1);

namespace Ergo\Module;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * @implements \IteratorAggregate<ModuleRoute>
 */
class ModuleRoutes implements IteratorAggregate
{
    /**
     * @var array<ModuleRoute>
     */
    private $routes;

    public function __construct(ModuleRoute ...$routes)
    {
        $this->routes = $routes;
    }

    /**
     * @return Traversable<ModuleRoute>
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->routes);
    }
}
