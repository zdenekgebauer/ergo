<?php

declare(strict_types=1);

namespace Ergo\Module;

use Ergo\Acl\Permissions;
use Ergo\Acl\Resource;
use Ergo\ApplicationInterface;
use Ergo\Html\Links;
use Ergo\Search\SearchIndexInterface;
use Ergo\Sitemap\SitemapInterface;
use ZdenekGebauer\Router\Routes;

abstract class Module implements ModuleInterface
{
    public function __construct(protected ApplicationInterface $application)
    {
    }

    abstract public function name(): string;

    abstract public static function internalName(): string;

    public function routes(): Routes
    {
        return new Routes();
    }

    public function inlineContent(string $parameter): string
    {
        if (str_starts_with($parameter, '_url_')) {
            return $this->application->getUrl(substr($parameter, 5));
        }
        return '';
    }

    public function cron(): void
    {
    }

    public function backendPermissions(): ?Permissions
    {
        return null;
    }

    public function frontendMenu(): ModuleRoutes
    {
        return new ModuleRoutes();
    }

    public function updateSitemap(SitemapInterface $sitemap): void
    {
    }

    public function updateSearchIndex(SearchIndexInterface $searchIndex): void
    {
    }

    public function registerRoutes(): void
    {
    }
}
