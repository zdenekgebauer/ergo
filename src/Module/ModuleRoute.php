<?php

declare(strict_types=1);

namespace Ergo\Module;

class ModuleRoute
{
    /**
     * @var string
     */
    private $module;

    /**
     * @var string
     */
    private $route;

    /**
     * @var string
     */
    private $name;

    public function __construct(string $module, string $route, string $name)
    {
        $this->module = $module;
        $this->route = $route;
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getModule(): string
    {
        return $this->module;
    }

    public function getRoute(): string
    {
        return $this->route;
    }
}
