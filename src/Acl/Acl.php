<?php

declare(strict_types=1);

namespace Ergo\Acl;

use LogicException;

class Acl implements AclInterface
{
    /**
     * @var array <Permission>
     */
    protected $allPermissions = [];

    /**
     * @var array <int, array<string>>
     */
    protected $grantedPermissions = [];

    public function registerPermissions(Permissions $permissions): void
    {
        foreach ($permissions as $permission) {
            if (isset($this->allPermissions[$permission->getId()])) {
                throw new LogicException('Permission "' . $permission->getId() . '" is already registered');
            }
            $this->allPermissions[$permission->getId()] = $permission;
        }
    }

    public function allPermissions(): array
    {
        return $this->allPermissions;
    }

    /**
     * @param int $userId
     * @param array<string> $permissionIds
     */
    public function grantPermission(int $userId, array $permissionIds): void
    {
        $permissionIds = array_map('\strval', $permissionIds);

        $that = $this;
        $permissionIds = array_filter(
            $permissionIds,
            static function ($permissionId) use ($that) {
                return isset($that->allPermissions[$permissionId]);
            }
        );
        $this->grantedPermissions[$userId] = $permissionIds;
    }

    public function hasPermission(int $userId, string $permissionId): bool
    {
        return in_array($permissionId, $this->grantedPermissions[$userId], true);
    }
}
