<?php

declare(strict_types=1);

namespace Ergo\Acl;

interface AclInterface
{
    public function registerPermissions(Permissions $permissions): void;

    /**
     * @return array<Permission>
     */
    public function allPermissions(): array;

    /**
     * @param int $userId
     * @param array<string> $permissionIds
     */
    public function grantPermission(int $userId, array $permissionIds): void;

    public function hasPermission(int $userId, string $permissionId): bool;
}
