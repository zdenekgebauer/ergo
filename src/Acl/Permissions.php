<?php

declare(strict_types=1);

namespace Ergo\Acl;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * @implements \IteratorAggregate<Permission>
 */
class Permissions implements IteratorAggregate
{
    /**
     * @var array<Permission>
     */
    private $permissions;

    public function __construct(Permission ...$permissions)
    {
        $this->permissions = $permissions;
    }

    /**
     * @return Traversable<Permission>
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->permissions);
    }
}
