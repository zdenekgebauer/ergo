<?php

declare(strict_types=1);

namespace Ergo\Acl;

class Permission
{
    public int $id;

    public string $name;

    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
    // TODO ZG slo by readonly pomoci __set?
}
