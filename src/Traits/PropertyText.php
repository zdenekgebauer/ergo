<?php

declare(strict_types=1);

namespace Ergo\Traits;

/**
 * longer text
 */
trait PropertyText
{
    /**
     * @var string
     */
    protected $text = '';

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }
}
