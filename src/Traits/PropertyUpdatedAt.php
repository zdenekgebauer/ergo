<?php

declare(strict_types=1);

namespace Ergo\Traits;

use DateTimeInterface;

trait PropertyUpdatedAt
{
    /**
     * @var DateTimeInterface
     */
    protected $updatedAt;

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt === null ? null : clone $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
