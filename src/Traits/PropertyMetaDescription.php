<?php

declare(strict_types=1);

namespace Ergo\Traits;

use ZdenekGebauer\Utils\Vars\StringUtils;

/**
 * properties meta description and keywords
 */
trait PropertyMetaDescription
{
    /**
     * @var string
     */
    protected $metaDescription = '';

    /**
     * @var string
     */
    protected $metaKeywords = '';

    public function getMetaDescription(): string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(string $metaDescription): void
    {
        $this->metaDescription = StringUtils::substr($metaDescription, 0, 185);
    }

    public function getMetaKeywords(): string
    {
        return $this->metaKeywords;
    }

    public function setMetaKeywords(string $metaKeywords): void
    {
        $this->metaKeywords = StringUtils::substr($metaKeywords, 0, 185);
    }
}
