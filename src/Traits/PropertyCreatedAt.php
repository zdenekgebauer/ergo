<?php

declare(strict_types=1);

namespace Ergo\Traits;

use DateTimeInterface;

trait PropertyCreatedAt
{
    /**
     * @var DateTimeInterface
     */
    protected $createdAt;

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt === null ? null : clone $this->createdAt;
    }

    public function setCreatedAt(?DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}
