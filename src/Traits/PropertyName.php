<?php

declare(strict_types=1);

namespace Ergo\Traits;

use ZdenekGebauer\Utils\Vars\StringUtils;

trait PropertyName
{
    protected $name = '';

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * max 100 chars
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = StringUtils::substr($name, 0, 100);
    }
}
