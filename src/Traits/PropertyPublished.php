<?php

declare(strict_types=1);

namespace Ergo\Traits;

/**
 * boolean flag "published" entity
 */
trait PropertyPublished
{
    /**
     * @var bool
     */
    protected $published = false;

    public function isPublished(): bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): void
    {
        $this->published = $published;
    }
}
