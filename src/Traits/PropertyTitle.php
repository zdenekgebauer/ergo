<?php

declare(strict_types=1);

namespace Ergo\Traits;

use ZdenekGebauer\Utils\Vars\StringUtils;

trait PropertyTitle
{
    protected $title = '';

    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * max 100 chars
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = StringUtils::substr($title, 0, 100);
    }
}
