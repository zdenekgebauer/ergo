<?php

declare(strict_types=1);

namespace Ergo\Traits;

use DateTimeInterface;

/**
 * date time of entity
 */
trait PropertyDate
{
    /**
     * @var DateTimeInterface
     */
    protected $date;

    public function getDate(): ?DateTimeInterface
    {
        return $this->date instanceof DateTimeInterface ? clone $this->date : null;
    }

    public function setDate(?DateTimeInterface $dateTime): void
    {
        $this->date = $dateTime;
    }

    public function getDateFormatted(string $format): string
    {
        return $this->date === null ? '' : $this->date->format($format);
    }
}
