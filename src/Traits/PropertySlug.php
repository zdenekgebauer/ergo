<?php

declare(strict_types=1);

namespace Ergo\Traits;

use ZdenekGebauer\Utils\Vars\StringUtils;

trait PropertySlug
{
    /**
     * @var string
     */
    private $slug = '';

    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * max 102 chars
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = substr(StringUtils::slugify($slug), 0, 102);
    }
}
