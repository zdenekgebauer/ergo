<?php

declare(strict_types=1);

namespace Ergo\Traits;

trait PropertyPerex
{
    /**
     * @var string
     */
    protected $perex = '';

    public function getPerex(): string
    {
        return $this->perex;
    }

    public function setPerex(string $perex): void
    {
        $this->perex = $perex;
    }
}
