<?php

declare(strict_types=1);

namespace Ergo\Traits;

use ZdenekGebauer\Utils\Vars\StringUtils;

trait PropertyEmail
{
    protected $email = '';

    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * max 100 chars
     * @param string $name
     */
    public function setEmail(string $name): void
    {
        $this->email = StringUtils::substr($name, 0, 100);
    }
}
