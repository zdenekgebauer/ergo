<?php

declare(strict_types=1);

namespace Ergo\Container;

interface ContainerInterface extends \Psr\Container\ContainerInterface
{
    /**
     * @param string $abstract
     * @param object|null $concrete
     */
    public function set(string $abstract, $concrete = null): void;

    public function addSubstitute(string $className, object $object): void;

    /**
     * @param string $name
     * @param array<array> $rule
     */
    public function addRule(string $name, array $rule): void;

    /**
     * Returns true if the container contains an entry for the given identifier.
     * Returns false otherwise.
     *
     * @param string $id Identifier of the entry to look for.
     * @return bool
     */
    public function hasInstance(string $id): bool;
}
