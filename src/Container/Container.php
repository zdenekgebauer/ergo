<?php

/**
 * @noinspection PhpRedundantCatchClauseInspection
 * @noinspection BadExceptionsProcessingInspection
 */

declare(strict_types=1);

namespace Ergo\Container;

use Dice\Dice;
use ReflectionException;
use Throwable;

class Container implements ContainerInterface
{
    /**
     * @var array<object|string>
     */
    protected $instances = [];

    /**
     * @var Dice
     */
    private $dice;

    public function __construct()
    {
        $dice = new Dice();
        $this->dice = $dice->addRule('*', ['shared' => true]);
    }

    /**
     * @param string $abstract
     * @param object|null $concrete
     */
    public function set(string $abstract, $concrete = null): void
    {
        $this->instances[$abstract] = ($concrete ?? $abstract);
    }

    public function addSubstitute(string $className, object $object): void
    {
        $this->dice = $this->dice->addRule('*', ['substitutions' => [$className => $object], 'shared' => true]);
    }

    public function addRule(string $name, array $rule): void
    {
        $rule['shared'] = true;
        $this->dice = $this->dice->addRule($name, $rule);
    }

    /**
     * @param string $id
     * @return object|string
     * @throws ContainerException
     * @throws NotFoundException
     */
    public function get($id)
    {
        if (isset($this->instances[$id])) {
            return $this->instances[$id];
        }

        try {
            return $this->dice->create($id);
        } catch (ReflectionException $exception) {
            throw new NotFoundException($exception->getMessage(), (int)$exception->getCode(), $exception);
        } catch (Throwable $exception) {
            throw new ContainerException($exception->getMessage(), (int)$exception->getCode(), $exception);
        }
    }

    public function has(string $id): bool
    {
        $result = true;
        try {
            $this->get($id);
        } catch (NotFoundException $exception) {
            $result = false;
        } catch (Throwable $exception) {
            $result = true;
        }
        return $result;
    }

    public function hasInstance(string $id): bool
    {
        return isset($this->instances[$id]);
    }
}
