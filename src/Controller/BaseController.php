<?php

declare(strict_types=1);

namespace Ergo\Controller;

use Ergo\ApplicationInterface;
use Ergo\Http\RequestInterface;
use Ergo\Http\ResponseInterface;
use stdClass;

class BaseController
{
    public function __construct(protected ApplicationInterface $application)
    {
    }

    public function content(string $content, string $contentType, int $status = ResponseInterface::HTTP_OK): void
    {
        $this->application->setResponse($this->application->response()->content($content, $contentType, $status));
    }

    public function html(string $html, int $status = ResponseInterface::HTTP_OK): void
    {
        $this->application->setResponse($this->application->response()->html($html, $status));
    }

    public function xml(string $xml, int $status = ResponseInterface::HTTP_OK): void
    {
        $this->application->setResponse($this->application->response()->xml($xml, $status));
    }

    /**
     * @param stdClass|array<stdClass>|array<string> $json
     * @param int $status
     */
    public function json($json, int $status = ResponseInterface::HTTP_OK): void
    {
        $this->application->setResponse($this->application->response()->json($json, $status));
    }

    public function file(string $file): void
    {
        $this->application->setResponse($this->application->response()->file($file));
    }

    public function download(string $file): void
    {
        $this->application->setResponse($this->application->response()->download($file));
    }

    public function downloadData(string $data, string $fileName): void
    {
        $this->application->setResponse($this->application->response()->downloadData($data, $fileName));
    }

    public function redirect(string $url, int $status = ResponseInterface::HTTP_FOUND): void
    {
        $this->application->setResponse($this->application->response()->redirect($url, $status));
    }
}
