<?php

declare(strict_types=1);

namespace Ergo\Controller;

use Ergo\Http\Response;
use Ergo\Template\PageComposer;

class DefaultController extends BaseController
{
    public function renderDefaultPage(): void
    {
        $page = new PageComposer($this->application);
        $page->pageTemplate($this->application->template('main.htm'));
        $page->canonical($this->application->config()->baseUrl());
        $page->title('Ergo default page');
        $page->metaDescription('Ergo default page');
        $page->metaKeywords('Ergo, default, page');
        $this->html($page->render('<h1>default page</h1>'));
    }

    public function render404Page(): void
    {
        $page = new PageComposer($this->application);
        $page->pageTemplate($this->application->template('main.htm'));
        $page->title('Ergo: page not found');
        $this->html($page->render('<h1>page not found</h1>'), Response::HTTP_NOT_FOUND);
    }
}
