<?php

declare(strict_types=1);

namespace Ergo\Template;

interface PageComposerInterface
{
    public function title(string $title): void;

    public function metaDescription(string $metaDescription): void;

    public function metaKeywords(string $metaKeywords): void;

    public function canonical(string $url): void;
}
