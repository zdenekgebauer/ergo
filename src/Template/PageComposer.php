<?php

declare(strict_types=1);

namespace Ergo\Template;

use Ergo\ApplicationInterface;
use ZdenekGebauer\Template\Template;
use ZdenekGebauer\Utils\Html\Html;

class PageComposer implements PageComposerInterface
{
    /**
     * @var string
     */
    protected $pageTemplate = '';

    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var array<string, string>
     */
    protected $metaNames;

    /**
     * @var array<string, string>
     */
    protected $metaProperties;

    protected array $links = [];

    /**
     * @var array<string, string>
     */
    protected array $alternateLanguageLinks = [];

    public function __construct(protected ApplicationInterface $application)
    {
        $this->metaNames = array_fill_keys(['robots', 'description', 'keywords'], '');

        $this->metaProperties = array_fill_keys(
            [
                'og:title',
                'og:description',
                'og:url',
                'twitter:card',
                'twitter:title',
                'twitter:description',
            ],
            ''
        );
        $this->metaProperties['twitter:card'] = 'summary';
    }

    public function pageTemplate(string $pageTemplate): void
    {
        $this->pageTemplate = $pageTemplate;
    }

    public function render(string $mainContent): string
    {
        $tpl = new Template($this->pageTemplate, false);
        $this->modifyPageTemplate($tpl);
        $tpl->replaceRaw('ERGO_MAIN_OUTPUT', $mainContent);

        while (strpos($tpl->get(), '[%MOD_') !== false) {
            $tpl->set($this->replaceModules($tpl->get()));
        }

        $tpl->replaceRaw('ERGO_HEADER_OUTPUT', $this->contentHeader());
        $tpl->replaceRaw('URL_ROOT', $this->application->config()->baseUrl(), true);

        foreach ($this->application->getLanguages() as $language) {
            $tpl->replaceRaw(
                'URL_LANGUAGE_' . strtoupper($language),
                $this->alternateLanguageLinks[$language] ?? '',
                true
            );
        }
        return Html::shrinkHtml($tpl->get());
    }

    protected function modifyPageTemplate(Template $template): void
    {
    }

    public function contentHeader(): string
    {
        $ret = '<title>' . Html::escape($this->title) . "</title>\n";
        foreach ($this->metaNames as $name => $value) {
            if (!empty($value)) {
                $ret .= '<meta name="' . Html::escape($name) . '" content="' . Html::escape($value) . "\"/>\n";
            }
        }
        foreach ($this->metaProperties as $name => $value) {
            if (!empty($value)) {
                $ret .= '<meta property="' . Html::escape($name) . '" content="' . Html::escape($value) . "\"/>\n";
            }
        }

        foreach ($this->links as $parameters) {
            $attributes = [];
            foreach ($parameters as $key => $value) {
                $attributes[] = Html::escape($key) . '="' . Html::escape($value) . '"';
            }
            $ret .=  '<link ' . implode(' ', $attributes) . ">\n";
        }

        return trim($ret);
    }

    protected function replaceModules(string $string): string
    {
        $occurrences = [];
        preg_match_all('/\[%MOD_(.*)%]/U', $string, $occurrences, PREG_SET_ORDER);

        foreach ($occurrences as $item) {
            $tag = $item[0];
            $tmp = explode('#', $item[1]);
            $name = $tmp[0];
            $parameter = ($tmp[1] ?? '');

            $module = $this->application->module($name);
            $replacement = $module->inlineContent($parameter);
            $string = str_replace($tag, $replacement, $string);
        }
        return $string;
    }

    public function title(string $title): void
    {
        $this->title = $title;
        $this->metaProperty('og:title', $title);
        $this->metaProperty('twitter:title', $title);
    }

    public function metaDescription(string $metaDescription): void
    {
        $this->metaName('description', $metaDescription);
        $this->metaProperty('og:description', $metaDescription);
        $this->metaProperty('twitter:description', $metaDescription);
    }

    public function metaKeywords(string $metaKeywords): void
    {
        $this->metaName('keywords', $metaKeywords);
    }

    public function canonical(string $url): void
    {
        $this->headLink($url, ['rel' => 'canonical']);
    }

    /**
     * sets meta name tag
     *
     * @param string $name name of property
     * @param string $value value of property
     */
    public function metaName(string $name, string $value): void
    {
        $this->metaNames[$name] = $value;
    }

    /**
     * sets meta property tag
     *
     * @param string $name name of property
     * @param string $value value of property
     */
    public function metaProperty(string $name, string $value): void
    {
        $this->metaProperties[$name] = $value;
    }

    /**
     * sets meta "robots noindex"
     */
    public function metaNoIndex(): void
    {
        $this->metaName('robots', 'noindex');
    }

    protected function headLink(string $href, array $parameters): void
    {
        $this->links[] = array_merge($parameters, ['href' => $href]);
    }

    public function hrefLangUrl(string $href, string $language, array $parameters = []): void
    {
        $this->headLink($href, array_merge($parameters, ['rel' => 'alternate', 'hreflang' => $language]));
    }

    public function setAlternateLanguageLink(string $language, string $href): void
    {
        $this->alternateLanguageLinks[$language] = $href;
        $this->headLink($href, ['rel' => 'alternate', 'hreflang' => $language]);
    }
}
