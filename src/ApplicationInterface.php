<?php

declare(strict_types=1);

namespace Ergo;

use Ergo\Config\ConfigInterface;
use Ergo\Container\ContainerInterface;
use Ergo\Http\RequestInterface;
use Ergo\Http\ResponseInterface;
use Ergo\Module\ModuleInterface;

interface ApplicationInterface
{
    public function __construct(ConfigInterface $config);

    public function config(): ConfigInterface;

    public function run(RequestInterface $request): ResponseInterface;

    public function request(): RequestInterface;

    public function response(): ResponseInterface;

    public function setResponse(ResponseInterface $response): void;

    public function module(string $internalName): ModuleInterface;

    public function container(): ContainerInterface;

    /**
     * @template T
     * @param class-string<T> $className
     * @return T
     */
    public function getInstance(string $className): object;

    /**
     * @param array<string, int|string> $parameters
     */
    public function getUrl(string $routeName, array $parameters = []): string;

    public function template(string $filename, ?string $moduleClassName = null): string;

    public function registerRoute(string $uri, callable $handler, string $name = '', array|string $method = 'GET');
}
