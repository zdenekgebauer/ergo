<?php

declare(strict_types=1);

namespace Ergo\Html;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * @implements \IteratorAggregate<Link>
 */
class Links implements IteratorAggregate
{
    /**
     * @var array<Link>
     */
    private $links;

    public function __construct(Link ...$links)
    {
        $this->links = $links;
    }

    /**
     * @return Traversable<Link>
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->links);
    }
}
