<?php

declare(strict_types=1);

namespace Ergo\Html;

class Link
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $title;

    public function __construct(string $name, string $url, string $title = '')
    {
        $this->name = $name;
        $this->url = $url;
        $this->title = $title;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
