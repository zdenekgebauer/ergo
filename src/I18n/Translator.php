<?php

declare(strict_types=1);

namespace Ergo\I18n;

class Translator implements TranslatorInterface
{
    private const TOKEN = 'TOKEN';

    /**
     * @var array<string, string>
     */
    protected $translations = [];

    /**
     * Read translations from language files.
     *
     * Lines in files have to be in format 'token;value'.
     * Lines starting with `#` will be ignored as comments.
     *
     * @param array<string> $files
     */
    public function __construct(array $files)
    {
        foreach ($files as $file) {
            $lines = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
            if (is_array($lines)) {
                foreach ($lines as $line) {
                    $line = trim($line);
                    if (empty($line) || strpos($line, '#') === 0) {
                        continue;
                    }
                    $translation = explode(';', $line, 2);
                    $translation = array_map('trim', $translation);
                    if (!empty($translation[1])) {
                        $this->translations[$translation[0]] = $translation[1];
                    }
                }
            }
        }
    }

    /**
     * returns translated text by given token
     *
     * If translation is not found, raise notice and returns token
     *
     * @param string $token
     * @return string
     */
    public function translate(string $token): string
    {
        $token = trim(ltrim($token, '{%'), '%}');
        if (isset($this->translations[$token])) {
            return $this->translations[$token];
        }
        user_error('translation not found: "' . $token . '"', E_USER_NOTICE);
        return $token;
    }

    /**
     * Returns text with placeholders replaced by translations
     *
     * Placeholder template must contains string TOKEN
     *
     * @param string $text
     * @param string $placeholderTemplate
     * @return string
     */
    public function translateAll(string $text, string $placeholderTemplate = '{%TOKEN%}'): string
    {
        $occurrences = self::findPlaceholders($text, $placeholderTemplate);
        foreach ($occurrences as $occurrence) {
            $replace = str_replace(self::TOKEN, $occurrence, $placeholderTemplate);
            $text = str_replace($replace, $this->translate($occurrence), $text);
        }
        return $text;
    }

    /**
     * find placeholders to translate in given text
     *
     * Placeholder template must contains string TOKEN
     *
     * @param string $text
     * @param string $placeholderTemplate
     * @return array<string>
     */
    public static function findPlaceholders(string $text, string $placeholderTemplate = '{%TOKEN%}'): array
    {
        $occurrences = [];
        $regexp = str_replace(self::TOKEN, '(.*)', preg_quote($placeholderTemplate, '/'));
        preg_match_all('/' . $regexp . '/U', $text, $occurrences, PREG_PATTERN_ORDER);
        return array_unique($occurrences[1]);
    }
}
