<?php

declare(strict_types=1);

namespace Ergo\I18n;

interface TranslatorInterface
{
    /**
     * returns translated text by given token
     * If translation not found, raise notice and returns token
     *
     * @param string $token key in language file (first column)
     * @return string translated text
     */
    public function translate(string $token): string;

    /**
     * returns given text with tags (i.e. {%SOMETHING%}) replaced by translations
     *
     * @param string $text
     * @return string
     */
    public function translateAll(string $text): string;
}
