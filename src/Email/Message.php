<?php

declare(strict_types=1);

namespace Ergo\Email;

class Message
{
    /**
     * @var Address
     */
    private $sender;

    /**
     * @var array<Address>
     */
    private $recipients;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $bodyPlaintext;

    /**
     * @var string
     */
    private $bodyHtml;

    /**
     * @var Attachment[]
     */
    private $attachments = [];

    /**
     * @param Address $sender
     * @param Address|Address[] $recipients
     * @param string $subject
     * @param string $bodyPlaintext
     * @param string $bodyHtml
     */
    public function __construct(
        Address $sender,
        $recipients,
        string $subject,
        string $bodyPlaintext,
        string $bodyHtml = ''
    ) {
        $this->sender = $sender;

        $recipients = is_array($recipients) ? $recipients : [$recipients];
        array_walk(
            $recipients,
            static function (Address $address) {
            }
        );
        $this->recipients = $recipients;
        $this->subject = $subject;
        $this->bodyPlaintext = $bodyPlaintext;
        $this->bodyHtml = $bodyHtml;
    }

    /**
     * @return Address
     */
    public function getSender(): Address
    {
        return $this->sender;
    }

    /**
     * @return Address[]
     */
    public function getRecipients(): array
    {
        return $this->recipients;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getBodyPlaintext(): string
    {
        return $this->bodyPlaintext;
    }

    public function getBodyHtml(): string
    {
        return $this->bodyHtml;
    }

    /**
     * @return Attachment[]
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }

    public function addAttachment(Attachment $attachment): void
    {
        $this->attachments[] = $attachment;
    }
}
