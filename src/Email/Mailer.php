<?php

declare(strict_types=1);

namespace Ergo\Email;

use PHPMailer\PHPMailer\PHPMailer;

class Mailer implements MailerInterface
{
    /**
     * @var PHPMailer
     */
    private $phpMailer;

    public function __construct(PHPMailer $phpMailer)
    {
        $this->phpMailer = $phpMailer;
    }

    public function send(Message $message): void
    {
        $this->phpMailer->setFrom($message->getSender()->getEmail(), $message->getSender()->getName());
        $this->phpMailer->clearAddresses();
        foreach ($message->getRecipients() as $recipient) {
            $this->phpMailer->addAddress($recipient->getEmail(), $recipient->getName());
        }
        // @codingStandardsIgnoreStart
        $this->phpMailer->Subject = $message->getSubject();
        if (empty($message->getBodyHtml())) {
            $this->phpMailer->Body = $message->getBodyPlaintext();
        } else {
            $this->phpMailer->Body = $message->getBodyHtml();
            $this->phpMailer->AltBody = $message->getBodyPlaintext();
        }
        // @codingStandardsIgnoreEnd
        $this->phpMailer->isHTML(!empty($message->getBodyHtml()));

        $this->phpMailer->clearAttachments();
        foreach ($message->getAttachments() as $attachment) {
            if ($attachment instanceof AttachmentFile) {
                $this->phpMailer->addAttachment(
                    $attachment->getPath(),
                    $attachment->getFilename(),
                    $attachment->getEncoding()
                );
            }
            if ($attachment instanceof AttachmentString) {
                $this->phpMailer->addStringAttachment(
                    $attachment->getString(),
                    $attachment->getFilename(),
                    $attachment->getEncoding()
                );
            }
        }

        $this->phpMailer->send();
    }
}
