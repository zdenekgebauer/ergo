<?php

declare(strict_types=1);

namespace Ergo\Email;

class AttachmentString extends Attachment
{
    /**
     * @var string
     */
    private $string;

    public function __construct(string $string, string $filename, string $encoding = 'base64', string $mimeType = '')
    {
        parent::__construct($filename, $encoding, $mimeType);
        $this->string = $string;
    }

    public function getString(): string
    {
        return $this->string;
    }
}
