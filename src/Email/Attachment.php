<?php

declare(strict_types=1);

namespace Ergo\Email;

abstract class Attachment
{
    /**
     * @var string
     */
    private $filename;

    /**
     * @var string
     */
    private $encoding;

    /**
     * @var string
     */
    private $mimetype;

    public function __construct(string $filename, string $encoding = 'base64', string $mimeType = '')
    {
        $this->filename = $filename;
        $this->encoding = $encoding;
        $this->mimetype = $mimeType;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getEncoding(): string
    {
        return $this->encoding;
    }

    public function getMimetype(): string
    {
        return $this->mimetype;
    }
}
