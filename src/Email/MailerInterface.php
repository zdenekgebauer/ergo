<?php

declare(strict_types=1);

namespace Ergo\Email;

interface MailerInterface
{
    public function send(Message $message): void;
}
