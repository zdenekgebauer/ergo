<?php

declare(strict_types=1);

namespace Ergo\Email;

class AttachmentFile extends Attachment
{
    /**
     * @var string
     */
    private $path;

    public function __construct(string $path, string $filename, string $encoding = 'base64', string $mimeType = '')
    {
        parent::__construct($filename, $encoding, $mimeType);
        $this->path = $path;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): void
    {
        $this->path = $path;
    }
}
