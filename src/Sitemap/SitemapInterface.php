<?php

declare(strict_types=1);

namespace Ergo\Sitemap;

use DateTimeInterface;
use ZdenekGebauer\SitemapBuilder\Item;

interface SitemapInterface
{
    /**
     * @param string $moduleClassName
     * @param array<Item> $items
     */
    public function addItems(string $moduleClassName, array $items): void;

    public function removeOldItems(string $moduleClassName, DateTimeInterface $limit): void;
}
