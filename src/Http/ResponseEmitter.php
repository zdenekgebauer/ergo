<?php

declare(strict_types=1);

namespace Ergo\Http;

use Psr\Http\Message\ResponseInterface;
use RuntimeException;

class ResponseEmitter
{
    /**
     * @param ResponseInterface $response
     * @param int $maxBufferSize Maximum output buffering size (pass 0 to emit all content at once)
     */
    public function emit(ResponseInterface $response, int $maxBufferSize = 8192): void
    {
        $this->assertNoPreviousOutput();
        $this->emitHeaders($response);
        $this->emitStatus($response);
        $this->emitBody($response, $maxBufferSize);
    }

    /**
     * Checks that no headers have been sent / the output buffer contains no content
     *
     * @codeCoverageIgnore
     */
    protected function assertNoPreviousOutput(): void
    {
        if (headers_sent()) {
            throw new RuntimeException('Unable to emit response; headers already sent.');
        }

        if (ob_get_level() > 0 && ob_get_length() > 0) {
            throw new RuntimeException('Unable to emit response; output has been emitted previously.');
        }
    }

    protected function emitHeaders(ResponseInterface $response): void
    {
        $statusCode = $response->getStatusCode();
        foreach ($response->getHeaders() as $name => $values) {
            foreach ($values as $value) {
                $this->emitHeader(sprintf('%s: %s', $name, $value), false, $statusCode);
            }
        }
    }

    /**
     * @param string $header
     * @param bool $replace
     * @param int $statusCode
     * @codeCoverageIgnore
     */
    protected function emitHeader(string $header, bool $replace, int $statusCode): void
    {
        header($header, $replace, $statusCode);
    }

    protected function emitStatus(ResponseInterface $response): void
    {
        $statusCode = $response->getStatusCode();
        $reasonPhrase = $response->getReasonPhrase();

        $header = sprintf(
            'HTTP/%s %d%s',
            $response->getProtocolVersion(),
            $statusCode,
            ($reasonPhrase ? ' ' . $reasonPhrase : '')
        );

        $this->emitHeader($header, true, $statusCode);
    }

    /**
     * Emits the message body
     *
     * @param ResponseInterface $response
     * @param int $maxBufferSize
     */
    protected function emitBody(ResponseInterface $response, int $maxBufferSize): void
    {
        $stream = $response->getBody();

        if (!$maxBufferSize || !$stream->isReadable()) {
            echo $stream;
            return;
        }

        if ($stream->isSeekable()) {
            $stream->rewind();
        }

        while (!$stream->eof()) {
            echo $stream->read($maxBufferSize);
        }
    }
}
