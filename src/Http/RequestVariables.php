<?php

declare(strict_types=1);

namespace Ergo\Http;

class RequestVariables
{
    /**
     * @param string|int|float $value
     * @return string
     */
    public static function asString($value): string
    {
        return trim(strip_tags(self::asHtml($value)));
    }

    /**
     * @param string|int|float $value
     * @return string
     */
    public static function asHtml($value): string
    {
        mb_substitute_character('none');
        return trim((string)mb_convert_encoding((string)$value, 'UTF-8', 'UTF-8'));
    }

    /**
     * @param string|int|float $value
     * @return bool
     */
    public static function asBool($value): bool
    {
        return (bool)filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * @param array<string> $value
     * @return array<string>
     */
    public static function asArray(array $value): array
    {
        return array_map([self::class, 'asString'], $value);
    }

    /**
     * @param array<string> $value
     * @return array<string>
     */
    public static function asArrayHtml(array $value): array
    {
        return array_map([self::class, 'asHtml'], $value);
    }
}
