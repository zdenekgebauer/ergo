<?php

declare(strict_types=1);

namespace Ergo\Http;

use DateTimeImmutable;
use Psr\Http\Message\ServerRequestInterface;

interface RequestInterface extends ServerRequestInterface
{
    public function currentUrl(): string;

    public function getQuery(string $name): string;

    public function getPost(string $name): string;

    public function getCookie(string $name): string;

    public function getQueryInt(string $name): int;

    public function getPostInt(string $name): int;

    public function getQueryFloat(string $name): float;

    public function getPostFloat(string $name): float;

    public function getQueryBool(string $name): bool;

    public function getPostBool(string $name): bool;

    public function getQueryDate(string $name): ?DateTimeImmutable;

    public function getPostDate(string $name): ?DateTimeImmutable;

    public function getPostHtml(string $name): string;

    /**
     * @return array<string>
     */
    public function getPostArray(string $name): array;

    /**
     * @return array<string>
     */
    public function getPostArrayHtml(string $name): array;

    /**
     * @return array<string>
     */
    public function getQueryArray(string $name): array;

    /**
     * @return array<int>
     */
    public function getPostArrayInt(string $name): array;

    /**
     * @return array<int>
     */
    public function getQueryArrayInt(string $name): array;

    public function isAjax(): bool;
}
