<?php

declare(strict_types=1);

namespace Ergo\Http;

use DateTimeImmutable;
use GuzzleHttp\Psr7\CachingStream;
use GuzzleHttp\Psr7\LazyOpenStream;
use GuzzleHttp\Psr7\ServerRequest;
use Psr\Http\Message\ServerRequestInterface;
use ZdenekGebauer\Utils\Vars\DateUtils;
use ZdenekGebauer\Utils\Vars\NumberUtils;

use function is_array;

final class Request extends ServerRequest implements RequestInterface
{
    /**
     * @return RequestInterface
     */
    public static function fromGlobals(): ServerRequestInterface
    {
        $method = $_SERVER['REQUEST_METHOD'] ?? 'GET';
        $body = new CachingStream(new LazyOpenStream('php://input', 'r+'));
        $protocol = isset($_SERVER['SERVER_PROTOCOL']) ? str_replace('HTTP/', '', $_SERVER['SERVER_PROTOCOL']) : '1.1';
        $serverRequest = new self($method, self::getUriFromGlobals(), getallheaders(), $body, $protocol, $_SERVER);

        return $serverRequest
            ->withCookieParams($_COOKIE)
            ->withQueryParams($_GET ?? [])
            ->withParsedBody($_POST ?? [])
            ->withUploadedFiles(self::normalizeFiles($_FILES));
    }

    public function currentUrl(): string
    {
        $uri = self::getUriFromGlobals();
        return $uri->getScheme() . '://' . $uri->getAuthority() . $uri->getPath();
    }

    public function getQuery(string $name): string
    {
        return RequestVariables::asString($this->getQueryParams()[$name] ?? '');
    }

    public function getPost(string $name): string
    {
        return RequestVariables::asString($this->getPostHtml($name));
    }

    public function getCookie(string $name): string
    {
        return RequestVariables::asString($this->getCookieParams()[$name] ?? '');
    }

    public function getQueryInt(string $name): int
    {
        return NumberUtils::intval($this->getQuery($name));
    }

    public function getPostInt(string $name): int
    {
        return NumberUtils::intval($this->getPost($name));
    }

    public function getQueryFloat(string $name): float
    {
        return NumberUtils::floatval($this->getQuery($name));
    }

    public function getPostFloat(string $name): float
    {
        return NumberUtils::floatval($this->getPost($name));
    }

    public function getQueryBool(string $name): bool
    {
        return RequestVariables::asBool($this->getQuery($name));
    }

    public function getPostBool(string $name): bool
    {
        return RequestVariables::asBool($this->getPost($name));
    }

    public function getQueryDate(string $name): ?DateTimeImmutable
    {
        return DateUtils::toDateTime($this->getQuery($name));
    }

    public function getPostDate(string $name): ?DateTimeImmutable
    {
        return DateUtils::toDateTime($this->getPost($name));
    }

    public function getPostHtml(string $name): string
    {
        $post = $this->getParsedBody();
        return RequestVariables::asHtml(is_array($post) && isset($post[$name]) ? $post[$name] : '');
    }

    /**
     * @param string $name
     * @return array<string>
     */
    public function getPostArray(string $name): array
    {
        $post = $this->getParsedBody();
        return RequestVariables::asArray(
            is_array($post) && isset($post[$name]) && is_array($post[$name]) ? $post[$name] : []
        );
    }

    /**
     * @return array<string>
     */
    public function getPostArrayHtml(string $name): array
    {
        $post = $this->getParsedBody();
        return RequestVariables::asArrayHtml(
            is_array($post) && isset($post[$name]) && is_array($post[$name]) ? $post[$name] : []
        );
    }

    /**
     * @return array<string>
     */
    public function getQueryArray(string $name): array
    {
        $query = $this->getQueryParams();
        return RequestVariables::asArray(
            is_array($query) && isset($query[$name]) && is_array($query[$name]) ? $query[$name] : []
        );
    }

    /**
     * @return array<int>
     */
    public function getPostArrayInt(string $name): array
    {
        return array_map([NumberUtils::class, 'intval'], $this->getPostArray($name));
    }

    /**
     * @return array<int>
     */
    public function getQueryArrayInt(string $name): array
    {
        return array_map([NumberUtils::class, 'intval'], $this->getQueryArray($name));
    }

    /**
     * true if exists any data in $_POST
     *
     * @return bool
     */
    public function hasPost(): bool
    {
        return !empty($this->getParsedBody());
    }

    public function isAjax(): bool
    {
        return strtolower($this->getServerParam('HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest';
    }

    public function getServerParam(string $parameter): string
    {
        return $this->getServerParams()[$parameter] ?? '';
    }
}
