<?php

declare(strict_types=1);

namespace Ergo\Http;

use stdClass;

interface ResponseInterface extends \Psr\Http\Message\ResponseInterface
{
    public const HTTP_OK = 200;
    public const HTTP_MOVED_PERMANENTLY = 301;
    public const HTTP_FOUND = 302;
    public const HTTP_SEE_OTHER = 303;
    public const HTTP_BAD_REQUEST = 400;
    public const HTTP_UNAUTHORIZED = 401;
    public const HTTP_FORBIDDEN = 403;
    public const HTTP_NOT_FOUND = 404;
    public const HTTP_INTERNAL_SERVER_ERROR = 500;

    public function content(
        string $content,
        string $contentType,
        int $status = ResponseInterface::HTTP_OK
    ): ResponseInterface;

    public function html(string $html, int $status = self::HTTP_OK): ResponseInterface;

    public function xml(string $xml, int $status = self::HTTP_OK): ResponseInterface;

    /**
     * @param stdClass|array<stdClass>|array<string> $json
     * @param int $status
     * @return ResponseInterface
     */
    public function json($json, int $status = self::HTTP_OK): ResponseInterface;

    public function download(string $file): ResponseInterface;

    public function downloadData(string $data, string $fileName): ResponseInterface;

    public function file(string $file): ResponseInterface;

    public function redirect(string $url, int $status = self::HTTP_FOUND): ResponseInterface;
}
