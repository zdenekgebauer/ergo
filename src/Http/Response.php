<?php

declare(strict_types=1);

namespace Ergo\Http;

use GuzzleHttp\Psr7\MimeType;
use GuzzleHttp\Psr7\Utils;
use stdClass;

class Response extends \GuzzleHttp\Psr7\Response implements ResponseInterface
{
    public function content(
        string $content,
        string $contentType,
        int $status = ResponseInterface::HTTP_OK
    ): ResponseInterface {
        return $this->withStatus($status)
            ->withHeader('Content-Type', $contentType)
            ->withBody(Utils::streamFor($content));
    }

    public function html(string $html, int $status = ResponseInterface::HTTP_OK): ResponseInterface
    {
        return $this->content($html, 'text/html; charset=utf-8', $status);
    }

    public function xml(string $xml, int $status = ResponseInterface::HTTP_OK): ResponseInterface
    {
        return $this->content($xml, 'application/xml', $status);
    }

    /**
     * @param stdClass|array<stdClass>|array<string> $json
     * @param int $status
     * @return ResponseInterface
     */
    public function json($json, int $status = ResponseInterface::HTTP_OK): ResponseInterface
    {
        return $this->content(json_encode($json, JSON_THROW_ON_ERROR), 'application/json', $status);
    }

    public function download(string $file): ResponseInterface
    {
        return $this->withHeader('Content-Disposition', 'attachment; filename="' . basename($file) . '"')
            ->withHeader('Content-Type', (string)MimeType::fromFilename($file))
            ->withHeader('Content-Length', (string)filesize($file))
            ->withBody(Utils::streamFor(Utils::tryFopen($file, 'rb')));
    }

    public function downloadData(string $data, string $fileName): ResponseInterface
    {
        return $this->withHeader('Content-Disposition', 'attachment; filename="' . $fileName . '"')
            ->withHeader('Content-Type', (string)MimeType::fromFilename($fileName))
            ->withHeader('Content-Length', (string)strlen($data))
            ->withBody(Utils::streamFor(stream_for($data)));
    }

    public function file(string $file): ResponseInterface
    {
        return $this->withHeader('Content-Type', (string)MimeType::fromFilename($file))
            ->withHeader('Content-Length', (string)filesize($file))
            ->withBody(Utils::streamFor(Utils::tryFopen($file, 'rb')));
    }

    public function redirect(string $url, int $status = ResponseInterface::HTTP_FOUND): ResponseInterface
    {
        return $this->withStatus($status)
            ->withHeader('Location', $url);
    }
}
