<?php

declare(strict_types=1);

namespace Ergo\Exceptions;

use RuntimeException;
use Throwable;

class ValidationException extends RuntimeException
{
    /**
     * @var array<string>
     */
    private $messages;

    /**
     * @param array<string> $messages
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(array $messages, int $code = 0, Throwable $previous = null)
    {
        $this->messages = array_map('\strval', $messages);
        parent::__construct((string)reset($this->messages), $code, $previous);
    }

    /**
     * @return array<string>
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    public function getMessageLine(): string
    {
        return implode(', ', $this->messages);
    }
}
