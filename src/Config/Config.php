<?php

declare(strict_types=1);

namespace Ergo\Config;

use BadMethodCallException;
use InvalidArgumentException;

use function in_array;
use function is_array;
use function is_string;

class Config implements ConfigInterface
{
    /**
     * @var array<string, mixed>
     */
    protected $configuration;

    public function __construct(string $rootDirectory)
    {
        $rootDirectory = rtrim($rootDirectory, '/') . '/';
        $this->configuration = [ConfigInterface::PARAMETER_ROOT_DIRECTORY => $rootDirectory];
        $this->addParameter(ConfigInterface::PARAMETER_MODE, ConfigInterface::MODE_DEVELOPMENT);
        $this->addParameter('timezone', date_default_timezone_get());
        $this->addParameter(ConfigInterface::PARAMETER_LOG_DIRECTORY, $rootDirectory . 'log/');
        $this->addParameter('templates_directories', [$rootDirectory . 'app/templates/']);
    }

    public function addConfigFile(string $file): void
    {
        $loader = new IniLoader();
        $loader->loadFromFile($this, $file);
    }

    /**
     * @param string $parameter
     * @param mixed $value
     */
    public function addParameter(string $parameter, $value): void
    {
        $value = $this->normalizeParameter($parameter, $value);
        $this->assertValidValue($parameter, $value);
        $this->configuration[$parameter] = $value;
    }

    /**
     * @param string $parameter
     * @return bool|float|int|string
     */
    public function getValue(string $parameter)
    {
        if (isset($this->configuration[$parameter])) {
            if (is_array($this->configuration[$parameter])) {
                throw new BadMethodCallException(
                    'configuration "' . $parameter . '" is array, use method getValueAsArray()'
                );
            }
            return $this->configuration[$parameter];
        }
        throw new InvalidArgumentException('configuration "' . $parameter . '" not found');
    }

    /**
     * @param string $parameter
     * @return array<string>
     */
    public function getValueAsArray(string $parameter): array
    {
        if (isset($this->configuration[$parameter])) {
            $values = $this->configuration[$parameter];
            return array_map('trim', is_array($values) ? $values : [(string)$values]);
        }
        throw new InvalidArgumentException('configuration "' . $parameter . '" not found');
    }

    public function baseDir(): string
    {
        return (string)$this->getValue(ConfigInterface::PARAMETER_ROOT_DIRECTORY);
    }

    public function baseUrl(): string
    {
        return (string)$this->getValue('root_url');
    }

    public function dataDir(): string
    {
        return (string)$this->getValue(ConfigInterface::PARAMETER_DATA_DIRECTORY);
    }

    public function templatesDir(): string
    {
        return (string)$this->getValue(ConfigInterface::PARAMETER_TEMPLATES_DIRECTORY);
    }

    public function logsDir(): string
    {
        return (string)$this->getValue(ConfigInterface::PARAMETER_LOG_DIRECTORY);
    }

    public function isDevelopment(): bool
    {
        return $this->getValue(ConfigInterface::PARAMETER_MODE) === ConfigInterface::MODE_DEVELOPMENT;
    }

    public function isProduction(): bool
    {
        return $this->getValue(ConfigInterface::PARAMETER_MODE) === ConfigInterface::MODE_PRODUCTION;
    }

    public function defaultTimezone(): string
    {
        return (string)$this->getValue('timezone');
    }

    /**
     * @param string $parameter
     * @param mixed $value
     * @return mixed
     */
    private function normalizeParameter(string $parameter, $value)
    {
        $rootDirectory = $this->baseDir();

        if (is_string($value)) {
            $value = str_replace('%root_directory%', $rootDirectory, $value);
        }
        if (is_array($value)) {
            $value = array_map(static function ($item) use ($rootDirectory) {
                return is_string($item) ? str_replace('%root_directory%', $rootDirectory, $item) : $item;
            }, $value);
        }
        if ($parameter === ConfigInterface::PARAMETER_LOG_DIRECTORY) {
            $value = (string)(is_array($value) ? reset($value) : $value);
            $value = rtrim($value, '/') . '/';
        }
        return $value;
    }

    /**
     * @param string $parameter
     * @param mixed $value
     */
    private function assertValidValue(string $parameter, $value): void
    {
        if ($parameter === ConfigInterface::PARAMETER_MODE) {
            $validValues = [
                ConfigInterface::MODE_PRODUCTION,
                ConfigInterface::MODE_TESTING,
                ConfigInterface::MODE_DEVELOPMENT,
            ];
            if (!in_array($value, $validValues, true)) {
                throw new InvalidArgumentException(
                    'invalid value "' . print_r($value, true) . '" for "' . $parameter . '"'
                );
            }
        }
    }
}
