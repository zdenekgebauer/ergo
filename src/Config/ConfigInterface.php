<?php

declare(strict_types=1);

namespace Ergo\Config;

/**
 * interface for application configuration
 */
interface ConfigInterface
{
    public const PARAMETER_ROOT_DIRECTORY = 'root_directory';

    public const PARAMETER_MODE = 'mode';

    public const PARAMETER_DATA_DIRECTORY = 'data_directory',
        PARAMETER_LOG_DIRECTORY = 'log_directory',
        PARAMETER_TEMPLATES_DIRECTORY = 'templates_directory';

    /**
     * run mode
     */
    public const MODE_PRODUCTION = 'production';

    public const MODE_TESTING = 'testing';

    public const MODE_DEVELOPMENT = 'development';

    /**
     * @param string $rootDirectory base directory of web
     */
    public function __construct(string $rootDirectory);

    /**
     * @param string $parameter
     * @param mixed $value
     */
    public function addParameter(string $parameter, $value): void;

    /**
     * @param string $parameter
     * @return bool|float|int|string
     */
    public function getValue(string $parameter);

    /**
     * @param string $parameter
     * @return array<string>
     */
    public function getValueAsArray(string $parameter): array;

    /**
     * @return string absolute path to root directory incl. trailing slash
     */
    public function baseDir(): string;

    /**
     * @return string absolute path to base  directory with user data, incl. trailing slash
     */
    public function dataDir(): string;

    /**
     * @return string absolute url to root of web incl. trailing slash
     */
    public function baseUrl(): string;

    /**
     * @return string absolute path to root directory with templates incl. trailing slash
     */
    public function templatesDir(): string;

    /**
     * @return string absolute path to root directory with logs, incl. trailing slash
     */
    public function logsDir(): string;

    public function isDevelopment(): bool;

    public function isProduction(): bool;

    public function defaultTimezone(): string;
}
