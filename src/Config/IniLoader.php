<?php

declare(strict_types=1);

namespace Ergo\Config;

use RuntimeException;

class IniLoader
{
    public function loadFromFile(ConfigInterface $config, string $file): void
    {
        if (!is_file($file)) {
            throw new RuntimeException('not found file ' . $file);
        }
        $iniArray = parse_ini_file($file, false, INI_SCANNER_TYPED);
        if (empty($iniArray)) {
            return;
        }
        foreach ($iniArray as $parameter => $value) {
            $config->addParameter($parameter, $value);
        }
    }
}
