<?php

declare(strict_types=1);

namespace Ergo\Cron;

use DateInterval;
use DateTime;
use DateTimeZone;
use Ergo\ApplicationInterface;

class Logger
{
    /**
     * @var ApplicationInterface
     */
    private $application;

    public function __construct(ApplicationInterface $application)
    {
        $this->application = $application;
    }

    public function saveLastRun(string $token): void
    {
        $now = new DateTime('now', new DateTimeZone('UTC'));
        file_put_contents($this->getLogFile($token), $now->format('c'));
    }

    private function getLogFile(string $token): string
    {
        return $this->application->config()->logsDir() . 'cron/' . $token . '.lastrun';
    }

    public function lastRunBefore(string $token, DateInterval $interval): bool
    {
        $lastRun = $this->getLastRun($token);
        $now = (new DateTime('now', new DateTimeZone('UTC')))->sub($interval);
        return $lastRun < $now;
    }

    private function getLastRun(string $token): ?DateTime
    {
        $fullPath = $this->getLogFile($token);

        if (!is_file($fullPath)) {
            return null;
        }
        $lastRun = trim((string)file_get_contents($fullPath));
        return new DateTime($lastRun);
    }
}
