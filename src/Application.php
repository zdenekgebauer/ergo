<?php

declare(strict_types=1);

namespace Ergo;

use Ergo\Config\ConfigInterface;
use Ergo\Container\Container;
use Ergo\Container\ContainerInterface;
use Ergo\Controller\DefaultController;
use Ergo\Http\RequestInterface;
use Ergo\Http\Response;
use Ergo\Http\ResponseInterface;
use Ergo\Module\ModuleInterface;
use InvalidArgumentException;
use Psr\Http\Message\MessageInterface;
use ZdenekGebauer\Router\Route;
use ZdenekGebauer\Router\Router;

use function in_array;

class Application implements ApplicationInterface
{
    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var ResponseInterface|Response|MessageInterface
     */
    protected $response;

    protected ContainerInterface $container;

    /**
     * @var array<ModuleInterface>
     */
    protected $modules = [];

    protected Router $router;

    public function __construct(ConfigInterface $config)
    {
        $this->config = $config;

        $this->container = new Container();
        $this->container->addSubstitute(ApplicationInterface::class, $this);
        $this->router = new Router();
        $this->response = new Response();
    }

    public function run(RequestInterface $request): ResponseInterface
    {
        $this->request = $request;
        $this->dispatchRequest($request);
        return $this->response;
    }

    public function config(): ConfigInterface
    {
        return clone $this->config;
    }

    public function container(): ContainerInterface
    {
        return $this->container;
    }

    public function getInstance(string $className): object
    {
        return $this->container->get($className);
    }

    protected function dispatchRequest(RequestInterface $request): void
    {
        $requestUri = $request->getUri()->getPath();
        $requestUri = str_replace((string)$this->config->getValue('base_path'), '', $requestUri);

        $match = $this->router->match($requestUri, $request->getMethod());
        if ($match) {
            $this->beforeInvokeHandler($match);
            $match->invokeHandler();
            return;
        }

        if ($requestUri === '/') {
            $this->renderDefaultPage();
            return;
        }
        $this->render404();
    }

    protected function beforeInvokeHandler(Route $route): void
    {
    }

    protected function renderDefaultPage(): void
    {
        $this->container->get(DefaultController::class)->renderDefaultPage();
    }

    public function render404(): void
    {
        $this->container->get(DefaultController::class)->render404Page();
    }

    /**
     * @param class-string $moduleClassName
     */
    public function registerModule(string $moduleClassName): void
    {
        if (!in_array(ModuleInterface::class, (array)class_implements($moduleClassName), true)) {
            throw new InvalidArgumentException(
                'class ' . $moduleClassName . ' must implement ' . ModuleInterface::class
            );
        }

        $module = $this->getInstance($moduleClassName);
        $module->registerRoutes();
        $this->modules[$module->internalName()] = $module;
    }

    public function registerRoute(string $uri, callable $handler, string $name = '', array|string $method = Route::GET)
    {
        $this->router->addRoute(new Route($uri, $method, $handler, $name));
    }

    public function module(string $internalName): ModuleInterface
    {
        if (isset($this->modules[$internalName])) {
            return $this->modules[$internalName];
        }
        throw new InvalidArgumentException('unknown module "' . $internalName . '"');
    }

    public function response(): ResponseInterface
    {
        return $this->response;
    }

    public function request(): RequestInterface
    {
        return $this->request;
    }

    public function setResponse(ResponseInterface $response): void
    {
        $this->response = $response;
    }

    /**
     * @return array<ModuleInterface>
     */
    public function getModules(): array
    {
        return $this->modules;
    }

    public function getUrl(string $routeName, array $parameters = []): string
    {
        return trim($this->config->baseUrl(), '/') . $this->router->createUrl($routeName, $parameters);
    }

    public function template(string $filename, ?string $moduleClassName = null): string
    {
        return $this->config->templatesDir() . $filename;
    }
}
