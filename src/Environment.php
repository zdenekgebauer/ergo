<?php

declare(strict_types=1);

namespace Ergo;

use Ergo\Config\ConfigInterface;
use Tracy\Debugger;

use function is_array;

final class Environment
{
    public static function setEnvironment(ConfigInterface $config): void
    {
        date_default_timezone_set($config->defaultTimezone());
        if (strpos((string)ini_get('disable_functions'), 'ini_set') === false) {
            ini_set('session.auto_start', '0');
            if (!headers_sent()) {
                ini_set('session.use_cookies', '1');
                ini_set('session.cookie_lifetime', '0');
                ini_set('session.use_only_cookies', '1');
                ini_set('session.cookie_httponly', '1');
                ini_set('session.use_trans_sid', '0');
                if (
                    (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off')
                    || (isset($_SERVER['SERVER_PORT']) &&  (int)$_SERVER['SERVER_PORT'] === 443)
                ) {
                    ini_set('session.cookie_secure', '1');
                }
                ini_set('session.cookie_samesite', 'Strict');
            }
            ini_set('url_rewriter.tags', '');
        }
    }

    public static function setErrorHandlers(ConfigInterface $config): void
    {
        $developerCookies = self::getDeveloperCookies($config);
        $errorEmails = self::getErrorEmails($config);
        $tracyMode = ($config->isDevelopment() ? Debugger::DEVELOPMENT : Debugger::PRODUCTION);
        $tracyMode = empty($developerCookies) ? $tracyMode : $developerCookies;
        $logDirectory = $config->getValue(ConfigInterface::PARAMETER_LOG_DIRECTORY) . 'err/';

        // force Tracy to display warnings
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
            $_SERVER['HTTP_X_TRACY_AJAX'] = 'really need log warnings';
        }

        /** @noinspection DisallowWritingIntoStaticPropertiesInspection */
        Debugger::$strictMode = true;
        Debugger::$logSeverity = E_ALL;
        Debugger::enable($tracyMode, $logDirectory, $errorEmails);
    }

    /**
     * @param ConfigInterface $config
     * @return array<string>
     */
    private static function getDeveloperCookies(ConfigInterface $config): array
    {
        $developerCookies = $config->getValueAsArray('developer_ip');
        return array_filter(
            $developerCookies,
            static function ($cookie) {
                $parts = explode('@', $cookie, 2);
                return !empty($parts[0]) && (filter_var($parts[1], FILTER_VALIDATE_IP) === $parts[1]);
            }
        );
    }

    /**
     * @param ConfigInterface $config
     * @return array<string>|null
     */
    private static function getErrorEmails(ConfigInterface $config): ?array
    {
        $errorEmails = $config->getValueAsArray('error_email');
        $errorEmails = filter_var_array($errorEmails, FILTER_VALIDATE_EMAIL, false);
        return is_array($errorEmails) ? array_unique($errorEmails) : null;
    }
}
